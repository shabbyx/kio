/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KIO_H
#define KIO_H

/*
 * The following are borrowed from linux/kernel.h
 * - size_t
 * - NULL
 * - __printf(x, y)
 * - __stringify(x)
 */
#include <linux/kernel.h>

/*
 * In linux/kernel.h, __printf(x,y) is defined, but __scanf(x,y) is not.
 * Here, we make sure both are defined as they should be.
 */
#ifndef __printf
# define __printf(x, y) __attribute__((format(printf, x, y)))
#endif
#ifndef __scanf
# define __scanf(x, y) __attribute__((format(scanf, x, y)))
#endif

/*
 * With C99, the function signatures have `restrict` in them
 * With other C versions, this keyword is simply dropped
 */
#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
# define KIO_RESTRICT restrict
#else
# define KIO_RESTRICT
#endif

/*
 * `EOKF` is the equivalent of `EOF` for `KFILE`s
 */
#define EOKF (-1)

/* the `KFILE` structure is the equivalent of `FILE` */
struct _KIO_FILE;
typedef struct _KIO_FILE KFILE;

void kclose(KFILE *stream);
void kflush(KFILE *stream);
void keouf(KFILE *stream);
KFILE *kopen(const char * KIO_RESTRICT filename, const char * KIO_RESTRICT mode);

int kprintf(KFILE * KIO_RESTRICT stream, const char * KIO_RESTRICT format, ...) __printf(2, 3);
int kscanf(KFILE * KIO_RESTRICT stream, const char * KIO_RESTRICT format, ...) __scanf(2, 3);
int vkprintf(KFILE * KIO_RESTRICT stream, const char * KIO_RESTRICT format, va_list arg) __printf(2, 0);
int vkscanf(KFILE * KIO_RESTRICT stream, const char * KIO_RESTRICT format, va_list arg) __scanf(2, 0);

int kgetc(KFILE * stream);
char *kgets(char * KIO_RESTRICT s, int n, KFILE * KIO_RESTRICT stream);
int kputc(int c, KFILE *stream);
int kputs(char * KIO_RESTRICT s, KFILE * KIO_RESTRICT stream);
int kungetc(int c, KFILE *stream);

size_t kread(void * KIO_RESTRICT ptr, size_t size, size_t nmemb, KFILE * KIO_RESTRICT stream);
size_t kwrite(const void * KIO_RESTRICT ptr, size_t size, size_t nmemb, KFILE * KIO_RESTRICT stream);

void kclearerr(KFILE *stream);
int keokf(KFILE *stream);
int kerror(KFILE *stream);
int krerror(KFILE *stream);
int kwerror(KFILE *stream);

#endif
