Test description
================

This program tests `kgetc`.  It creates two files `TEXT_FILE` and `BIN_FILE`
where the first is opened in `"pr"` and the second in `"prb"` mode.  These
file names are defined in `test_info.h`.

The user space program opens the two files and sends first `TEXT_NUM_CHARS`
characters cycling from `TEXT_RANGE_BEGIN` through `TEXT_RANGE_END`.  It then
proceeds to do the same with the binary file (with `BIN_*` macros instead of
`TEXT_*`).  These values are defined in `test_info.h`.

Correspondingly, the kernel starts reading `TEXT_NUM_CHARS` from the first
file, expecting characters in proper order and expecting end of file when tried
to read more.  It then proceeds to do the same with the second file.

In the end, it will expect a read from the file to return with a failure.

The user space program will return `EXIT_SUCCESS` if all operations were
successful and `EXIT_FAILURE` otherwise.  The kernel module will log
"kgetc -- passed" or "kgetc -- failed" based on whether the test was successful
or not.
