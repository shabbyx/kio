/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "test_info.h"

static int output(FILE *f, unsigned int count, char begin, char end)
{
	unsigned int i;
	char c = begin;
	for (i = 0; i < count; ++i)
	{
		if (fputc(c, f) == EOF)
			goto exit_bad_fputc;
		if (c == end)
			c = begin;
		else
			++c;
	}
	if (fflush(f) == EOF)
		goto exit_bad_fflush;
	return 0;
exit_bad_fputc:
	perror("error writing data");
	goto exit_fail;
exit_bad_fflush:
	perror("error flushing file");
	goto exit_fail;
exit_fail:
	return -1;
}

int main(void)
{
	FILE *fout = NULL;
	FILE *foutb = NULL;
	fout = fopen("/proc/"TEXT_FILE, "w");
	foutb = fopen("/proc/"BIN_FILE, "wb");
	if (fout == NULL || foutb == NULL)
		goto exit_no_file;
	if (output(fout, TEXT_NUM_CHARS, TEXT_RANGE_BEGIN, TEXT_RANGE_END))
		goto exit_fail;
	if (fclose(fout))
	{
		fout = NULL;
		goto exit_bad_fclose;
	}
	fout = NULL;	/* prevent double close */
	fprintf(stderr, "- text mode done\n");
	if (output(foutb, BIN_NUM_CHARS, BIN_RANGE_BEGIN, BIN_RANGE_END))
		goto exit_fail;
	if (fclose(foutb))
	{
		foutb = NULL;
		goto exit_bad_fclose;
	}
	fprintf(stderr, "- binary mode done\n");
	return EXIT_SUCCESS;
exit_no_file:
	perror("error opening files");
	goto exit_fail;
exit_bad_fclose:
	perror("error closing files");
	goto exit_fail;
exit_fail:
	if (fout)
		fclose(fout);
	if (foutb)
		fclose(foutb);
	return EXIT_FAILURE;
}
