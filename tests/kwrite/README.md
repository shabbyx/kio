Test description
================

This program tests `kwrite`.  It creates `BIN_FILE` opened in `"wbp"`.  This
file name is defined in `test_info.h`.

The kernel opens the file and sends `BIN_NUM_STRUCTS` structures of a struct of
`int`, `char *` and `unsigned short`.  The values of these fields cycle from
`BIN_RANGE_BEGIN_I/S/U` through `BIN_RANGE_END_I/S/U` respectively.  To further
test `kwrite`, the kernel will write the structures in chunks of 1 to
`BIN_CHUNK_COUNT`.

In the end, it will write EOKF and expect a write to the file to return with a failure.

Correspondingly, the user space program starts reading `BIN_NUM_STRUCTS`
structures from the file, expecting matching fields in proper order.  In the
end, it will expect end of file when tried to read more.

The user space program will return `EXIT_SUCCESS` if all operations were
successful and `EXIT_FAILURE` otherwise.  The kernel module will log
"kwrite -- passed" or "kwrite -- failed" based on whether the test was successful
or not.
