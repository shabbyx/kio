/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <kio.h>
#include "test_info.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Shahbaz Youssefi");

static struct task_struct *test_task;
static KFILE *pbout;

static int write_and_check(KFILE *out, unsigned int count, unsigned int max_chunk,
		int begin_i, int end_i, char *begin_s, char *end_s,
		unsigned short begin_u, unsigned short end_u)
{
	unsigned int i = 0, j;
	unsigned int cur_chunk = 1;
	struct test_data next = {
		.i = begin_i,
		.s = begin_s,
		.u = begin_u
	};
	struct test_data d[max_chunk];
	while (i < count)
	{
		unsigned int wrote;

		if (count - i < cur_chunk)
			cur_chunk = count - i;
		for (j = 0; j < cur_chunk; ++j)
		{
			d[j] = next;

			if (next.i == end_i)
				next.i = begin_i;
			else
				++next.i;
			if (next.s == end_s)
				next.s = begin_s;
			else
				++next.s;
			if (next.u == end_u)
				next.u = begin_u;
			else
				++next.u;
		}

		wrote = kwrite(d, sizeof(d[0]), cur_chunk, out);
		if (wrote < cur_chunk)
			goto exit_bad_write;

		i += cur_chunk;
		if (cur_chunk == max_chunk)
			cur_chunk = 1;
		else
			++cur_chunk;
	}
	keouf(out);
	if (kwrite(d, 1, 1, out) > 0)
		goto exit_bad_eouf;
	return 0;
exit_bad_write:
	printk(KERN_INFO "failed to write all data (at %uth data)\n", i);
	goto exit_fail;
exit_bad_eouf:
	printk(KERN_INFO "failed to stop writing after all data are finished\n");
	goto exit_fail;
exit_fail:
	return -1;
}

static int test_thread(void *arg)
{
	pbout = kopen(BIN_FILE, "wbp");
	if (pbout == NULL)
		goto exit_no_file;

	if (write_and_check(pbout, BIN_NUM_STRUCTS, BIN_CHUNK_COUNT,
				BIN_RANGE_BEGIN_I, BIN_RANGE_END_I,
				BIN_RANGE_BEGIN_S, BIN_RANGE_END_S,
				BIN_RANGE_BEGIN_U, BIN_RANGE_END_U))
		goto exit_fail;

	printk(KERN_INFO "kwrite -- passed\n");
	goto exit_normal;
exit_no_file:
	printk(KERN_INFO "failed to open file\n");
	goto exit_fail;
exit_fail:
	printk(KERN_INFO "kwrite -- failed\n");
exit_normal:
	if (pbout)
		kclose(pbout);
	pbout = NULL;
	do_exit(0);
	return 0;
}

static int __init test_init(void)
{
	if ((test_task = kthread_run(test_thread, NULL, "kwrite_test")) == ERR_PTR(-ENOMEM))
	{
		test_task = NULL;
		printk(KERN_INFO "initializing test... failed\n");
		return -ENOMEM;
	}
	printk(KERN_INFO "kwrite test loaded\n");
	return 0;
}

static void __exit test_exit(void)
{
	if (pbout)
		kclose(pbout);
	printk(KERN_INFO "kwrite test exited\n");
}

module_init(test_init);
module_exit(test_exit);
