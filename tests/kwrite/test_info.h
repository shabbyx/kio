/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEST_INFO_H
#define TEST_INFO_H

#define BIN_FILE "kwrite_test"

struct test_data
{
	int i;
	char *s;
	unsigned short u;
};

#define BIN_NUM_STRUCTS 1000000
#define BIN_CHUNK_COUNT 10
#define BIN_RANGE_BEGIN_I 27169
#define BIN_RANGE_END_I 975835
#define BIN_RANGE_BEGIN_S ((char *)0x02f28000)
#define BIN_RANGE_END_S ((char *)0x100000ff)
#define BIN_RANGE_BEGIN_U -12341
#define BIN_RANGE_END_U 9774

#endif
