/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "test_info.h"

static int input(FILE *f, unsigned int count,
		int begin_i, int end_i, char *begin_s, char *end_s,
		unsigned short begin_u, unsigned short end_u)
{
	unsigned int i;
	struct test_data expected = {
		.i = begin_i,
		.s = begin_s,
		.u = begin_u
	};
	struct test_data d;
	for (i = 0; i < count; ++i)
	{
		if (fread(&d, sizeof(d), 1, f) != 1)
			goto exit_bad_fread;

		if (d.i != expected.i || d.s != expected.s || d.u != expected.u)
			goto exit_mismatch;

		if (expected.i == end_i)
			expected.i = begin_i;
		else
			++expected.i;
		if (expected.s == end_s)
			expected.s = begin_s;
		else
			++expected.s;
		if (expected.u == end_u)
			expected.u = begin_u;
		else
			++expected.u;
	}
	if (fread(&d, sizeof(d), 1, f) > 0)
		goto exit_bad_eof;
	return 0;
exit_bad_fread:
	perror("error reading data");
	goto exit_fail;
exit_mismatch:
	fprintf(stderr, "failed to match with what was expected (at %uth data)", i);
	goto exit_fail;
exit_bad_eof:
	perror("error detecting eof after data were supposed to finish");
	goto exit_fail;
exit_fail:
	return -1;
}

int main(void)
{
	FILE *finb = NULL;
	finb = fopen("/proc/"BIN_FILE, "rb");
	if (finb == NULL)
		goto exit_no_file;
	if (input(finb, BIN_NUM_STRUCTS,
			BIN_RANGE_BEGIN_I, BIN_RANGE_END_I,
			BIN_RANGE_BEGIN_S, BIN_RANGE_END_S,
			BIN_RANGE_BEGIN_U, BIN_RANGE_END_U))
		goto exit_fail;
	if (fclose(finb))
	{
		finb = NULL;
		goto exit_bad_fclose;
	}
	return EXIT_SUCCESS;
exit_no_file:
	perror("error opening file");
	goto exit_fail;
exit_bad_fclose:
	perror("error closing file");
	goto exit_fail;
exit_fail:
	if (finb)
		fclose(finb);
	return EXIT_FAILURE;
}
