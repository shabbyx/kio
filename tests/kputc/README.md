Test description
================

This program tests `kputc`.  It creates two files `TEXT_FILE` and `BIN_FILE`
where the first is opened in `"pw"` and the second in `"pbw"` mode.  These
file names are defined in `test_info.h`.

The kernel sends first `TEXT_NUM_CHARS` characters cycling from
`TEXT_RANGE_BEGIN` through `TEXT_RANGE_END`.  It then proceeds to do the same
with the binary file (with `BIN_*` macros instead of `TEXT_*`).  These values
are defined in `test_info.h`.

In the end, it will write EOKF and expect a write to the file to return with a failure.

Correspondingly, the user space program starts reading `TEXT_NUM_CHARS` from the
first file, expecting characters in proper order and expecting end of file when
tried to read more.  It then proceeds to do the same with the second file.

The user space program will return `EXIT_SUCCESS` if all operations were
successful and `EXIT_FAILURE` otherwise.  The kernel module will log
"kputc -- passed" or "kputc -- failed" based on whether the test was successful
or not.
