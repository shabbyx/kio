/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <kio.h>
#include "test_info.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Shahbaz Youssefi");

static struct task_struct *test_task;
static KFILE *pout;
static KFILE *pbout;

static int write_and_check(KFILE *out, unsigned int count, char begin, char end)
{
	unsigned int i;
	char c = begin;
	for (i = 0; i < count; ++i)
	{
		if (kputc(c, out) == EOKF)
			goto exit_bad_write;
		if (c == end)
			c = begin;
		else
			++c;
	}
	keouf(out);
	if (kputc(c, out) != EOKF)
		goto exit_bad_eouf;
	return 0;
exit_bad_write:
	printk(KERN_INFO "failed to write all data (at %uth data)\n", i);
	goto exit_fail;
exit_bad_eouf:
	printk(KERN_INFO "failed to stop writing after all data are finished\n");
	goto exit_fail;
exit_fail:
	return -1;
}

static int test_thread(void *arg)
{
	pout = kopen(TEXT_FILE, "pw");
	pbout = kopen(BIN_FILE, "pbw");
	if (pout == NULL || pbout == NULL)
		goto exit_no_file;

	if (write_and_check(pout, TEXT_NUM_CHARS, TEXT_RANGE_BEGIN, TEXT_RANGE_END))
		goto exit_fail;
	printk(KERN_INFO "- text mode done\n");
	if (write_and_check(pbout, BIN_NUM_CHARS, BIN_RANGE_BEGIN, BIN_RANGE_END))
		goto exit_fail;
	printk(KERN_INFO "- binary mode done\n");

	printk(KERN_INFO "kputc -- passed\n");
	goto exit_normal;
exit_no_file:
	printk(KERN_INFO "failed to open file\n");
	goto exit_fail;
exit_fail:
	printk(KERN_INFO "kputc -- failed\n");
exit_normal:
	if (pout)
		kclose(pout);
	if (pbout)
		kclose(pbout);
	pout = NULL;
	pbout = NULL;
	do_exit(0);
	return 0;
}

static int __init test_init(void)
{
	if ((test_task = kthread_run(test_thread, NULL, "kputc_test")) == ERR_PTR(-ENOMEM))
	{
		test_task = NULL;
		printk(KERN_INFO "initializing test... failed\n");
		return -ENOMEM;
	}
	printk(KERN_INFO "kputc test loaded\n");
	return 0;
}

static void __exit test_exit(void)
{
	if (pout)
		kclose(pout);
	if (pbout)
		kclose(pbout);
	printk(KERN_INFO "kputc test exited\n");
}

module_init(test_init);
module_exit(test_exit);
