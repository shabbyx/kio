/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "test_info.h"

static int input(FILE *f, unsigned int count, char begin, char end)
{
	unsigned int i;
	int c;
	char expected = begin;
	for (i = 0; i < count; ++i)
	{
		c = fgetc(f);
		if (c == EOF)
			goto exit_bad_fgetc;
		if ((unsigned char)c != expected)
			goto exit_mismatch;
		if (expected == end)
			expected = begin;
		else
			++expected;
	}
	if (fgetc(f) != EOF)
		goto exit_bad_eof;
	return 0;
exit_bad_fgetc:
	perror("error reading data");
	goto exit_fail;
exit_mismatch:
	fprintf(stderr, "failed to match with what was expected (at %uth data)", i);
	goto exit_fail;
exit_bad_eof:
	perror("error detecting eof after data were supposed to finish");
	goto exit_fail;
exit_fail:
	return -1;
}

int main(void)
{
	FILE *fin = NULL;
	FILE *finb = NULL;
	fin = fopen("/proc/"TEXT_FILE, "r");
	finb = fopen("/proc/"BIN_FILE, "rb");
	if (fin == NULL || finb == NULL)
		goto exit_no_file;
	if (input(fin, TEXT_NUM_CHARS, TEXT_RANGE_BEGIN, TEXT_RANGE_END))
		goto exit_fail;
	if (fclose(fin))
	{
		fin = NULL;
		goto exit_bad_fclose;
	}
	fin = NULL;	/* prevent double close */
	fprintf(stderr, "- text mode done\n");
	if (input(finb, BIN_NUM_CHARS, BIN_RANGE_BEGIN, BIN_RANGE_END))
		goto exit_fail;
	if (fclose(finb))
	{
		finb = NULL;
		goto exit_bad_fclose;
	}
	fprintf(stderr, "- binary mode done\n");
	return EXIT_SUCCESS;
exit_no_file:
	perror("error opening files");
	goto exit_fail;
exit_bad_fclose:
	perror("error closing files");
	goto exit_fail;
exit_fail:
	if (fin)
		fclose(fin);
	if (finb)
		fclose(finb);
	return EXIT_FAILURE;
}
