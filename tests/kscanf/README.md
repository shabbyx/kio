Test description
================

This program tests `kscanf`.  It creates `TEXT_FILE` opened in `"pr"`.  This
file name is defined in `test_info.h`.

The user space program opens the file and sends `TEXT_NUM_TESTS` strings cycling
from `TEXT_RANGE_BEGIN_X` through `TEXT_RANGE_END_X` as `sprintf`ed.  This test
is repeated for the following data types where `X` is any of `hhu`, `hd`, `zu`,
`lx`, `llo`, `#X`, `#lo`, `p`.  These tests are repeated twice.

It then proceeds to send `TEXT_NUM_CHARS` characters cycling from
`TEST_RANGE_BEGIN_C` through `TEST_RANGE_END_C`.  Afterwards, `TEXT_NUM_STRS`
strings cycling from `TEST_RANGE_BEGIN_S` through `TEST_RANGE_END_S`, composed of
characters of `TEST_RANGE_*_C` are sent.  This send is repeated three more times
with characters in ranges `TEST_RANGE_*_C`, `TEST_RANGE_*_C_1` and `TEST_RANGE_*_C_2`.

These values are defined in `test_info.h`.

Correspondingly, the kernel starts reading `TEXT_NUM_TESTS` numbers from the file,
expecting numbers in proper order.  This process is repeated for all the cases
above with the same conversion specifier, except `zi` is used for `zu`, `i` is
used for `#X` and `li` is used for `#lo`.  This process is repeated twice, with
the second time having `TEST_LIM` after `%` followed by the same specifier with
`*` modifier.

Afterwards, `TEXT_NUM_CHARS` are read, expecting to be matched in the proper
order.  Following that, `TEXT_NUM_STRS` strings are read and matched.  This test
is repeated three more times with first `TEST_LIM` after `%` followed by `%*s`,
then a `%[` matching `TEST_RANGE_*_C_1` and finally a `%[^` matching
`TEST_RANGE_*_C_2`.

In the end, it will expect a read from the file to return with a failure.

The user space program will return `EXIT_SUCCESS` if all operations were
successful and `EXIT_FAILURE` otherwise.  The kernel module will log
"kscanf -- passed" or "kscanf -- failed" based on whether the test was successful
or not.
