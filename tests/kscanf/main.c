/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "test_info.h"

static void next_str(char *str, const char *begin,
		const char *end, char low, char high)
{
	size_t len;
	char *p;

	if (strcmp(str, end) == 0)
	{
		strcpy(str, begin);
		return;
	}

	len = strlen(str);
	for (p = str + len - 1; p + 1 != str; --p)
		if (*p == high)
			*p = low;
		else
		{
			++*p;
			break;
		}
}

static const char *spaces[5] = {"", " ", "  ", "   ", "    "};

static int output(FILE *f)
{
	unsigned int i;
#define SEND(cnt, fmt, t, T, s)							\
	do {									\
		t n = TEXT_RANGE_BEGIN_##T;					\
		for (i = 0; i < cnt; ++i)					\
		{								\
			fprintf(f, fmt"%s", n, spaces[s * (rand() % 4 + 1)]);	\
			if (n == TEXT_RANGE_END_##T)				\
				n = TEXT_RANGE_BEGIN_##T;			\
			else							\
				++n;						\
		}								\
		if (ferror(f))							\
			goto exit_bad_fprintf;					\
		fprintf(stderr, "- \"%s\" done\n", fmt);			\
	} while (0)
#define SEND_S(T, e, s)								\
	do {									\
		char str[50] = TEXT_RANGE_BEGIN_S;				\
		for (i = 0; i < TEXT_NUM_STRS; ++i)				\
		{								\
			fprintf(f, "%s"e"%s", str, spaces[s * (rand() % 4 + 1)]);\
			next_str(str, TEXT_RANGE_BEGIN_S, TEXT_RANGE_END_S,	\
				TEXT_RANGE_BEGIN_##T, TEXT_RANGE_END_##T);	\
		}								\
		if (ferror(f))							\
			goto exit_bad_fprintf;					\
		fprintf(stderr, "- string %s done\n", #T);			\
	} while (0)
	SEND(TEXT_NUM_TESTS, "%hhu", unsigned char, HHU, 1);
	SEND(TEXT_NUM_TESTS, "%hd", short, HD, 1);
	SEND(TEXT_NUM_TESTS, "hello %zu", size_t, ZU, 1);
	SEND(TEXT_NUM_TESTS, "%lx", long, LX, 1);
	SEND(TEXT_NUM_TESTS, "%%%llo", unsigned long long, LLO, 1);
	SEND(TEXT_NUM_TESTS, "%#X", int, SX, 1);
	SEND(TEXT_NUM_TESTS, "%#lo", long, SLO, 1);
	SEND(TEXT_NUM_TESTS, "%p", char *, P, 1);
	/* repeat */
	SEND(TEXT_NUM_TESTS, "%hhu", unsigned char, HHU, 1);
	SEND(TEXT_NUM_TESTS, "%hd", short, HD, 1);
	SEND(TEXT_NUM_TESTS, "%zu", size_t, ZU, 1);
	SEND(TEXT_NUM_TESTS, "%lx", long, LX, 1);
	SEND(TEXT_NUM_TESTS, "%llo", unsigned long long, LLO, 1);
	SEND(TEXT_NUM_TESTS, "%#X", int, SX, 1);
	SEND(TEXT_NUM_TESTS, "%#lo", long, SLO, 1);
	SEND(TEXT_NUM_TESTS, "%p", char *, P, 1);

	SEND(TEXT_NUM_CHARS, "%c", char, C, 0);
	if (fflush(f) == EOF)
		goto exit_bad_fflush;

	SEND_S(C, "", 1);
	SEND_S(C, "", 1);
	SEND_S(C_1, "", 1);
	SEND_S(C_2, "yo", 1);
	return 0;
exit_bad_fprintf:
	perror("error writing data");
	goto exit_fail;
exit_bad_fflush:
	perror("error flushing file");
	goto exit_fail;
exit_fail:
	return -1;
}

int main(void)
{
	FILE *fout = NULL;
	srand(time(NULL));
	fout = fopen("/proc/"TEXT_FILE, "w");
	if (fout == NULL)
		goto exit_no_file;
	if (output(fout))
		goto exit_fail;
	if (fclose(fout))
	{
		fout = NULL;
		goto exit_bad_fclose;
	}
	return EXIT_SUCCESS;
exit_no_file:
	perror("error opening file");
	goto exit_fail;
exit_bad_fclose:
	perror("error closing file");
	goto exit_fail;
exit_fail:
	if (fout)
		fclose(fout);
	return EXIT_FAILURE;
}
