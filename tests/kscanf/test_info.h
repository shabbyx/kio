/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEST_INFO_H
#define TEST_INFO_H

#define TEXT_FILE "kscanf_test"

#define TEXT_NUM_TESTS 100000
#define TEXT_RANGE_BEGIN_HHU 102
#define TEXT_RANGE_END_HHU 245
#define TEXT_RANGE_BEGIN_HD -32767
#define TEXT_RANGE_END_HD -12231
#define TEXT_RANGE_BEGIN_ZU 1030971111u
#define TEXT_RANGE_END_ZU 1030982342u
#define TEXT_RANGE_BEGIN_LX -12345678
#define TEXT_RANGE_END_LX -12344567
#define TEXT_RANGE_BEGIN_LLO 123456789123453789ll
#define TEXT_RANGE_END_LLO 123456789123456789ll
#define TEXT_RANGE_BEGIN_SX 452345
#define TEXT_RANGE_END_SX 523423
#define TEXT_RANGE_BEGIN_SLO 1234
#define TEXT_RANGE_END_SLO 3241
#define TEXT_RANGE_BEGIN_P ((char *)0x12345678)
#define TEXT_RANGE_END_P ((char *)0x12346678)

#define TEXT_NUM_CHARS 100000
#define TEXT_RANGE_BEGIN_C '-'
#define TEXT_RANGE_END_C ']'
#define TEXT_NUM_STRS 1000000
#define TEXT_RANGE_BEGIN_S "ABC"
#define TEXT_RANGE_END_S "EFG"
#define TEXT_RANGE_BEGIN_C_1 '+'
#define TEXT_RANGE_END_C_1 '_'
#define TEXT_RANGE_BEGIN_C_2 'A'
#define TEXT_RANGE_END_C_2 'Z'

#define TEST_LIM 2
#define TEST_LIM_STR "2"

#endif
