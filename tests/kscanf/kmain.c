/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <kio.h>
#include "test_info.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Shahbaz Youssefi");

static struct task_struct *test_task;
static KFILE *pin;

static void next_str(char *str, const char *begin,
		const char *end, char low, char high)
{
	size_t len;
	char *p;

	if (strcmp(str, end) == 0)
	{
		strcpy(str, begin);
		return;
	}

	len = strlen(str);
	for (p = str + len - 1; p + 1 != str; --p)
		if (*p == high)
			*p = low;
		else
		{
			++*p;
			break;
		}
}

static int read_and_check(KFILE *in)
{
	unsigned int i;
	char str[100];
	char stre[100];
#define MATCH(cnt, fmtr, fmtw, l, t, T)						\
	do {									\
		t expected = TEXT_RANGE_BEGIN_##T;				\
		t n;								\
		for (i = 0; i < cnt; ++i)					\
		{								\
			if (kscanf(in, fmtr, &n) != 1)				\
				goto exit_bad_read;				\
			sprintf(str, fmtw, n);					\
			str[l] = '\0';						\
			sprintf(stre, fmtw, expected);				\
			stre[l] = '\0';						\
			if (strcmp(str, stre) != 0)				\
				goto exit_mismatch;				\
			if (expected == TEXT_RANGE_END_##T)			\
				expected = TEXT_RANGE_BEGIN_##T;		\
			else							\
				++expected;					\
		}								\
		if (kerror(in))							\
			goto exit_bad_read;					\
		printk(KERN_INFO "- \"%s\" done\n", fmtr);			\
	} while (0)
#define MATCH_S(fmtr, l, T)							\
	do {									\
		char stre_orig[100] = TEXT_RANGE_BEGIN_S;			\
		for (i = 0; i < TEXT_NUM_STRS; ++i)				\
		{								\
			if (kscanf(in, fmtr, str) != 1)				\
				goto exit_bad_read;				\
			strncpy(stre, stre_orig, l);				\
			stre[l] = '\0';						\
			if (strcmp(str, stre) != 0)				\
				goto exit_mismatch;				\
			next_str(stre_orig, TEXT_RANGE_BEGIN_S,			\
				TEXT_RANGE_END_S, TEXT_RANGE_BEGIN_##T,		\
				TEXT_RANGE_END_##T);				\
		}								\
		if (kerror(in))							\
			goto exit_bad_read;					\
		printk(KERN_INFO "- string %s done\n", #T);			\
	} while (0)
	MATCH(TEXT_NUM_TESTS, "%hhu", "%hhu", 99, unsigned char, HHU);
	MATCH(TEXT_NUM_TESTS, "%hd", "%hd", 99, short, HD);
	MATCH(TEXT_NUM_TESTS, " hello %zi", "%zu", 99, ssize_t, ZU);
	MATCH(TEXT_NUM_TESTS, "%lx", "%lx", 99, long, LX);
	MATCH(TEXT_NUM_TESTS, " %%%llo", "%llo", 99, unsigned long long, LLO);
	MATCH(TEXT_NUM_TESTS, "%i", "%#X", 99, int, SX);
	MATCH(TEXT_NUM_TESTS, "%li", "%#lo", 99, long, SLO);
	MATCH(TEXT_NUM_TESTS, "%p", "%p", 99, char *, P);

	MATCH(TEXT_NUM_TESTS, "%"TEST_LIM_STR"hhu%*u", "%hhu", TEST_LIM, unsigned char, HHU);
	MATCH(TEXT_NUM_TESTS, "%"TEST_LIM_STR"hd%*d", "%hd", TEST_LIM, short, HD);
	MATCH(TEXT_NUM_TESTS, "%"TEST_LIM_STR"zi%*i", "%zu", TEST_LIM, ssize_t, ZU);
	MATCH(TEXT_NUM_TESTS, "%"TEST_LIM_STR"lx%*x", "%lx", TEST_LIM, long, LX);
	MATCH(TEXT_NUM_TESTS, "%"TEST_LIM_STR"llo%*o", "%llo", TEST_LIM, unsigned long long, LLO);
	MATCH(TEXT_NUM_TESTS, "%"TEST_LIM_STR"i%*x", "%#X", TEST_LIM, int, SX);
	MATCH(TEXT_NUM_TESTS, "%"TEST_LIM_STR"li%*o", "%#lo", TEST_LIM, long, SLO);
	MATCH(TEXT_NUM_TESTS, "%"TEST_LIM_STR"p%*p", "%#lx", TEST_LIM, char *, P);

	MATCH(TEXT_NUM_CHARS, " %c", "%c", 1, char, C);

	MATCH_S("%s", 99, C);
	MATCH_S("%"TEST_LIM_STR"s%*s", TEST_LIM, C);
	MATCH_S(" %[]+-_-]", 99, C_1);
	MATCH_S(" %[^] -,/-@[\\^_`a-z{|}~[-A-]yo", 99, C_2);	/* note: [-A should only include [ */

	if (kscanf(in, "%77s", str) > 0)
		goto exit_bad_eokf;
	return 0;
exit_bad_read:
	printk(KERN_INFO "failed to read all data (at %uth data)\n", i);
	goto exit_fail;
exit_mismatch:
	printk(KERN_INFO "failed to match with what was expected (at %uth data)\n", i);
	goto exit_fail;
exit_bad_eokf:
	printk(KERN_INFO "failed to detect eokf after all data are finished\n");
	goto exit_fail;
exit_fail:
	return -1;
}

static int test_thread(void *arg)
{
	pin = kopen(TEXT_FILE, "pr");
	if (pin == NULL)
		goto exit_no_file;

	if (read_and_check(pin))
		goto exit_fail;

	printk(KERN_INFO "kscanf -- passed\n");
	goto exit_normal;
exit_no_file:
	printk(KERN_INFO "failed to open file\n");
	goto exit_fail;
exit_fail:
	printk(KERN_INFO "kscanf -- failed\n");
exit_normal:
	if (pin)
		kclose(pin);
	pin = NULL;
	do_exit(0);
	return 0;
}

static int __init test_init(void)
{
	if ((test_task = kthread_run(test_thread, NULL, "kscanf_test")) == ERR_PTR(-ENOMEM))
	{
		test_task = NULL;
		printk(KERN_INFO "initializing test... failed\n");
		return -ENOMEM;
	}
	printk(KERN_INFO "kscanf test loaded\n");
	return 0;
}

static void __exit test_exit(void)
{
	if (pin)
		kclose(pin);
	printk(KERN_INFO "kscanf test exited\n");
}

module_init(test_init);
module_exit(test_exit);
