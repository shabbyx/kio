/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "test_info.h"

static void next_str(char *str, const char *begin,
		const char *end, char low, char high)
{
	size_t len;
	char *p;

	if (strcmp(str, end) == 0)
	{
		strcpy(str, begin);
		return;
	}

	len = strlen(str);
	for (p = str + len - 1; p + 1 != str; --p)
		if (*p == high)
			*p = low;
		else
		{
			++*p;
			break;
		}
}

static int input(FILE *f)
{
	unsigned int i;
#define MATCH(cnt, fmt, t, T)							\
	do {									\
		t expected = TEXT_RANGE_BEGIN_##T;				\
		t n;								\
		for (i = 0; i < cnt; ++i)					\
		{								\
			if (fscanf(f, fmt, &n) != 1)				\
				goto exit_bad_fscanf;				\
			if (n != expected)					\
				goto exit_mismatch;				\
			if (expected == TEXT_RANGE_END_##T)			\
				expected = TEXT_RANGE_BEGIN_##T;		\
			else							\
				++expected;					\
		}								\
		if (ferror(f))							\
			goto exit_bad_fscanf;					\
		fprintf(stderr, "- \"%s\" done\n", fmt);			\
	} while (0)
#define MATCH_S(fmt, l)								\
	do {									\
		char stre[100] = TEXT_RANGE_BEGIN_S;				\
		char str[100];							\
		for (i = 0; i < TEXT_NUM_STRS; ++i)				\
		{								\
			char tmp = stre[l];					\
			stre[l] = '\0';						\
			if (fscanf(f, fmt, str) != 1)				\
				goto exit_bad_fscanf;				\
			if (strcmp(str, stre) != 0)				\
				goto exit_mismatch;				\
			stre[l] = tmp;						\
			next_str(stre, TEXT_RANGE_BEGIN_S,			\
				TEXT_RANGE_END_S, TEXT_RANGE_BEGIN_C,		\
				TEXT_RANGE_END_C);				\
		}								\
		if (ferror(f))							\
			goto exit_bad_fscanf;					\
		fprintf(stderr, "- \"%s\" done\n", fmt);			\
	} while (0)
	MATCH(TEXT_NUM_TESTS, "%hhu", unsigned char, HHU);
	MATCH(TEXT_NUM_TESTS, "%hd", short, HD);
	MATCH(TEXT_NUM_TESTS, " hello %zu", size_t, ZU);
	MATCH(TEXT_NUM_TESTS, "%lx", long, LX);
	MATCH(TEXT_NUM_TESTS, " %%%llo", unsigned long long, LLO);
	MATCH(TEXT_NUM_TESTS, "%X", int, SX);
	MATCH(TEXT_NUM_TESTS, "%lo", long, SLO);
	MATCH(TEXT_NUM_TESTS, "%p", char *, P);

	MATCH(TEXT_NUM_TESTS, "%hhu", unsigned char, HHU);
	MATCH(TEXT_NUM_TESTS, "%hd", short, HD);
	MATCH(TEXT_NUM_TESTS, "%zu", size_t, ZU);
	MATCH(TEXT_NUM_TESTS, "%lx", long, LX);
	MATCH(TEXT_NUM_TESTS, "%llo", unsigned long long, LLO);
	MATCH(TEXT_NUM_TESTS, "%X", int, SX);
	MATCH(TEXT_NUM_TESTS, "%lo", long, SLO);
	MATCH(TEXT_NUM_TESTS, "%p", char *, P);

	MATCH(TEXT_NUM_CHARS, " %c", char, C);

	MATCH_S("%s", 99);
	MATCH_S("%s", TEST_LIM);

	if (fscanf(f, "%u", &i) > 0)
		goto exit_bad_eof;
	return 0;
exit_bad_fscanf:
	perror("error reading data");
	goto exit_fail;
exit_mismatch:
	fprintf(stderr, "failed to match with what was expected (at %uth data)", i);
	goto exit_fail;
exit_bad_eof:
	perror("error detecting eof after data were supposed to finish");
	goto exit_fail;
exit_fail:
	return -1;
}

int main(void)
{
	FILE *fin = NULL;
	srand(time(NULL));
	fin = fopen("/proc/"TEXT_FILE, "r");
	if (fin == NULL)
		goto exit_no_file;
	if (input(fin))
		goto exit_fail;
	if (fclose(fin))
	{
		fin = NULL;
		goto exit_bad_fclose;
	}
	return EXIT_SUCCESS;
exit_no_file:
	perror("error opening file");
	goto exit_fail;
exit_bad_fclose:
	perror("error closing file");
	goto exit_fail;
exit_fail:
	if (fin)
		fclose(fin);
	return EXIT_FAILURE;
}
