/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <kio.h>
#include "test_info.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Shahbaz Youssefi");

static struct task_struct *test_task;
static KFILE *pout;

static void next_str(char *str, const char *begin,
		const char *end, char low, char high)
{
	size_t len;
	char *p;

	if (strcmp(str, end) == 0)
	{
		strcpy(str, begin);
		return;
	}

	len = strlen(str);
	for (p = str + len - 1; p + 1 != str; --p)
		if (*p == high)
			*p = low;
		else
		{
			++*p;
			break;
		}
}

static const char *spaces[2] = {"", " "};

static int write(KFILE *out)
{
	unsigned int i;
#define SEND(cnt, fmt, t, T, s)								\
	do {										\
		t n = TEXT_RANGE_BEGIN_##T;						\
		for (i = 0; i < cnt; ++i)						\
		{									\
			if (kprintf(out, fmt"%s" EXTRA_ARG, n, spaces[s]) < 0)		\
				goto exit_bad_write;					\
			if (n == TEXT_RANGE_END_##T)					\
				n = TEXT_RANGE_BEGIN_##T;				\
			else								\
				++n;							\
		}									\
		if (kerror(out))							\
			goto exit_bad_write;						\
		printk(KERN_INFO "- \"%s\" done\n", fmt);				\
	} while (0)
#define SEND_S(fmt, s)									\
	do {										\
		char str[50] = TEXT_RANGE_BEGIN_S;					\
		for (i = 0; i < TEXT_NUM_STRS; ++i)					\
		{									\
			if (kprintf(out, fmt"%s" EXTRA_ARG, str, spaces[s]) < 0)	\
				goto exit_bad_write;					\
			next_str(str, TEXT_RANGE_BEGIN_S, TEXT_RANGE_END_S,		\
				TEXT_RANGE_BEGIN_C, TEXT_RANGE_END_C);			\
		}									\
		if (kerror(out))							\
			goto exit_bad_write;						\
		printk(KERN_INFO "- \"%s\" done\n", fmt);				\
	} while (0)
#define EXTRA_ARG
	SEND(TEXT_NUM_TESTS, "%hhu", unsigned char, HHU, 1);
	SEND(TEXT_NUM_TESTS, "%hd", short, HD, 1);
	SEND(TEXT_NUM_TESTS, "hello %zu", size_t, ZU, 1);
	SEND(TEXT_NUM_TESTS, "%lx", long, LX, 1);
	SEND(TEXT_NUM_TESTS, "%%%llo", unsigned long long, LLO, 1);
	SEND(TEXT_NUM_TESTS, "%#X", int, SX, 1);
	SEND(TEXT_NUM_TESTS, "%#lo", long, SLO, 1);
	SEND(TEXT_NUM_TESTS, "%p", char *, P, 1);

	SEND(TEXT_NUM_TESTS, "%+ hhu", unsigned char, HHU, 1);
	SEND(TEXT_NUM_TESTS, "%07hd", short, HD, 1);
#undef EXTRA_ARG
#define EXTRA_ARG , 20
	SEND(TEXT_NUM_TESTS, "%.*zu", size_t, ZU, 1);
#undef EXTRA_ARG
#define EXTRA_ARG
	SEND(TEXT_NUM_TESTS, "% lx", long, LX, 1);
#undef EXTRA_ARG
#define EXTRA_ARG , 30
	SEND(TEXT_NUM_TESTS, "%-*llo", unsigned long long, LLO, 1);
#undef EXTRA_ARG
#define EXTRA_ARG
	SEND(TEXT_NUM_TESTS, "%#0.30X", int, SX, 1);
	SEND(TEXT_NUM_TESTS, "%#.20lo", long, SLO, 1);
	SEND(TEXT_NUM_TESTS, "%+ #0p", char *, P, 1);

	SEND(TEXT_NUM_CHARS, "%c", char, C, 0);

	SEND_S("%s", 1);
#undef EXTRA_ARG
#define EXTRA_ARG , 10, TEST_LIM
	SEND_S("%*.*s", 1);
	keouf(out);
	if (kprintf(out, "123 %d", 10) > 0)
		goto exit_bad_eouf;
	return 0;
exit_bad_write:
	printk(KERN_INFO "failed to write all data (at %uth data)\n", i);
	goto exit_fail;
exit_bad_eouf:
	printk(KERN_INFO "failed to stop writing after all data are finished\n");
	goto exit_fail;
exit_fail:
	return -1;
}

static int test_thread(void *arg)
{
	pout = kopen(TEXT_FILE, "pw");
	if (pout == NULL)
		goto exit_no_file;

	if (write(pout))
		goto exit_fail;

	printk(KERN_INFO "kprintf -- passed\n");
	goto exit_normal;
exit_no_file:
	printk(KERN_INFO "failed to open file\n");
	goto exit_fail;
exit_fail:
	printk(KERN_INFO "kprintf -- failed\n");
exit_normal:
	if (pout)
		kclose(pout);
	pout = NULL;
	do_exit(0);
	return 0;
}

static int __init test_init(void)
{
	if ((test_task = kthread_run(test_thread, NULL, "kprintf_test")) == ERR_PTR(-ENOMEM))
	{
		test_task = NULL;
		printk(KERN_INFO "initializing test... failed\n");
		return -ENOMEM;
	}
	printk(KERN_INFO "kprintf test loaded\n");
	return 0;
}

static void __exit test_exit(void)
{
	if (pout)
		kclose(pout);
	printk(KERN_INFO "kprintf test exited\n");
}

module_init(test_init);
module_exit(test_exit);
