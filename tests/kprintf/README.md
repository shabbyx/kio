Test description
================

This program tests `kprintf`.  It creates `TEXT_FILE` opened in `"pw"`.  This
file name is defined in `test_info.h`.

The kernel opens the file and sends `TEXT_NUM_TESTS` strings cycling from
`TEXT_RANGE_BEGIN_X` through `TEXT_RANGE_END_X` as `sprintf`ed.  This test
is repeated for the following data types where `X` is any of `hhu`, `hd`, `zu`,
`lx`, `llo`, `#X`, `#lo`, `p`.  These tests are repeated twice, with the second one
having the following flags added: `+ hhu`, `07hd`, `.*zu` (given 20), ` lx`,
`-*llo` (given 30), `#0.30X`, `#.20lo`, `+ #0p`

It then proceeds to send `TEXT_NUM_CHARS` characters cycling from
`TEST_RANGE_BEGIN_C` through `TEST_RANGE_END_C`.  Afterwards, `TEXT_NUM_STRS`
strings cycling from `TEST_RANGE_BEGIN_S` through `TEST_RANGE_END_S`, composed of
characters of `TEST_RANGE_*_C` are sent.  This process is repeated twice, with the
second time using `%*.*s` (given 10 and `TEST_LIM`).

In the end, it will write EOKF and expect a write to the file to return with a failure.

These values are defined in `test_info.h`.

Correspondingly, the user space program starts reading `TEXT_NUM_TESTS` numbers from
the file, expecting numbers in proper order.  This process is repeated twice.

Afterwards, `TEXT_NUM_CHARS` are read, expecting to be matched in the proper
order.  Following that, `TEXT_NUM_STRS` strings are read and matched.  This test
is repeated twice with the second time having `TEST_LIM` after `%`.

The user space program will return `EXIT_SUCCESS` if all operations were
successful and `EXIT_FAILURE` otherwise.  The kernel module will log
"kprintf -- passed" or "kprintf -- failed" based on whether the test was successful
or not.
