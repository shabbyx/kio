/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <kio.h>
#include "test_info.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Shahbaz Youssefi");

static struct task_struct *test_task;
static KFILE *pin;

static int read_and_check(KFILE *in, unsigned int count, unsigned int count2,
		unsigned int len, unsigned int begin, unsigned int end)
{
	unsigned int i;
	unsigned int expected = begin;
	char str[100];
	char stre[20];
	for (i = 0; i < count; ++i)
	{
		if (kgets(str, 100, in) == NULL)
			goto exit_bad_read;
		sprintf(stre, "%u\n", expected);
		if (strcmp(str, stre) != 0)
			goto exit_mismatch;
		if (expected == end)
			expected = begin;
		else
			++expected;
	}
	expected = begin;
	for (i = 0; i < count2; ++i)
	{
		if (kgets(str, len + 1, in) == NULL)
			goto exit_bad_read;
		sprintf(stre, "%u", expected);
		stre[len] = '\0';
		if (strcmp(str, stre) != 0)
			goto exit_mismatch;
		if (expected == end)
			expected = begin;
		else
			++expected;
	}
	if (kgets(str, 100, in) != NULL)
		goto exit_bad_eokf;
	return 0;
exit_bad_read:
	printk(KERN_INFO "failed to read all data (at %uth data)\n", i);
	goto exit_fail;
exit_mismatch:
	printk(KERN_INFO "failed to match with what was expected (at %uth data)\n", i);
	goto exit_fail;
exit_bad_eokf:
	printk(KERN_INFO "failed to detect eokf after all data are finished\n");
	goto exit_fail;
exit_fail:
	return -1;
}

static int test_thread(void *arg)
{
	pin = kopen(TEXT_FILE, "pr");
	if (pin == NULL)
		goto exit_no_file;

	if (read_and_check(pin, TEXT_NUM_LINES, TEXT_NUM_NON_LINES, TEXT_NON_LINE_LEN,
				TEXT_RANGE_BEGIN, TEXT_RANGE_END))
		goto exit_fail;

	printk(KERN_INFO "kgets -- passed\n");
	goto exit_normal;
exit_no_file:
	printk(KERN_INFO "failed to open file\n");
	goto exit_fail;
exit_fail:
	printk(KERN_INFO "kgets -- failed\n");
exit_normal:
	if (pin)
		kclose(pin);
	pin = NULL;
	do_exit(0);
	return 0;
}

static int __init test_init(void)
{
	if ((test_task = kthread_run(test_thread, NULL, "kgets_test")) == ERR_PTR(-ENOMEM))
	{
		test_task = NULL;
		printk(KERN_INFO "initializing test... failed\n");
		return -ENOMEM;
	}
	printk(KERN_INFO "kgets test loaded\n");
	return 0;
}

static void __exit test_exit(void)
{
	if (pin)
		kclose(pin);
	printk(KERN_INFO "kgets test exited\n");
}

module_init(test_init);
module_exit(test_exit);
