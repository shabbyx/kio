Test description
================

This program tests `kgets`.  It creates `TEXT_FILE` opened in `"pr"`.  This
file name is defined in `test_info.h`.

The user space program opens the file and sends `TEXT_NUM_LINES` lines cycling
from `TEXT_RANGE_BEGIN` through `TEXT_RANGE_END` as `sprintf`ed.  It then
proceeds to send `TEXT_NUM_NON_LINES` strings cycling in the same range and
truncated to `TEXT_NON_LINE_LEN` characters.  These values are defined in
`test_info.h`.

Correspondingly, the kernel starts reading `TEXT_NUM_LINES` lines from the file,
expecting strings in proper order.  It then proceeds to read
`TEXT_NUM_NON_LINES` strings of length `TEXT_NON_LINE_LEN` expecting them in
proper order.

In the end, it will expect a read from the file to return with a failure.

The user space program will return `EXIT_SUCCESS` if all operations were
successful and `EXIT_FAILURE` otherwise.  The kernel module will log
"kgets -- passed" or "kgets -- failed" based on whether the test was successful
or not.
