/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "test_info.h"

static int output(FILE *f, unsigned int count, unsigned int count2, unsigned int len,
		unsigned int begin, unsigned int end)
{
	unsigned int i;
	unsigned int n = begin;
	char str[20];
	for (i = 0; i < count; ++i)
	{
		sprintf(str, "%u\n", n);
		if (fputs(str, f) == EOF)
			goto exit_bad_fputs;
		if (n == end)
			n = begin;
		else
			++n;
	}
	if (fflush(f) == EOF)
		goto exit_bad_fflush;
	n = begin;
	for (i = 0; i < count2; ++i)
	{
		sprintf(str, "%u", n);
		str[len] = '\0';
		if (fputs(str, f) == EOF)
			goto exit_bad_fputs;
		if (n == end)
			n = begin;
		else
			++n;
	}
	return 0;
exit_bad_fputs:
	perror("error writing data");
	goto exit_fail;
exit_bad_fflush:
	perror("error flushing file");
	goto exit_fail;
exit_fail:
	return -1;
}

int main(void)
{
	FILE *fout = NULL;
	fout = fopen("/proc/"TEXT_FILE, "w");
	if (fout == NULL)
		goto exit_no_file;
	if (output(fout, TEXT_NUM_LINES, TEXT_NUM_NON_LINES, TEXT_NON_LINE_LEN,
		TEXT_RANGE_BEGIN, TEXT_RANGE_END))
		goto exit_fail;
	if (fclose(fout))
	{
		fout = NULL;
		goto exit_bad_fclose;
	}
	return EXIT_SUCCESS;
exit_no_file:
	perror("error opening file");
	goto exit_fail;
exit_bad_fclose:
	perror("error closing file");
	goto exit_fail;
exit_fail:
	if (fout)
		fclose(fout);
	return EXIT_FAILURE;
}
