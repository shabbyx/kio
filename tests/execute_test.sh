#! /bin/bash

# Usage: ./execute_test.sh number-of-files

if ! $(lsmod | grep -w kio > /dev/null 2>&1); then
  sudo insmod $top_builddir/src/kio.ko
fi

# insert module and wait for it to create the files
sudo insmod kio_test.ko
while [ $(find /proc -maxdepth 1 -name "k*_test*" 2> /dev/null | wc -l) -ne $1 ]; do sleep 0.2; done

# run the feeder to read from/write to those files
./feeder
ret=$?

# wait a bit to make sure the module has finished with its tests too
sleep 0.5
sudo rmmod kio_test

exit $ret
