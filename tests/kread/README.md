Test description
================

This program tests `kread`.  It creates `BIN_FILE` opened in `"bpr"`.  This
file name is defined in `test_info.h`.

The user space program opens the file and sends `BIN_NUM_STRUCTS` structures of
a struct of `int`, `char *` and `unsigned short`.  The values of these fields
cycle from `BIN_RANGE_BEGIN_I/S/U` through `BIN_RANGE_END_I/S/U` respectively.

Correspondingly, the kernel starts reading `BIN_NUM_STRUCTS` structures from the
file, expecting matching fields in proper order.  To further test `kread`, the
kernel will read the structures in chunks of 1 to `BIN_CHUNK_COUNT`.

In the end, it will expect a read from the file to return with a failure.

The user space program will return `EXIT_SUCCESS` if all operations were
successful and `EXIT_FAILURE` otherwise.  The kernel module will log
"kread -- passed" or "kread -- failed" based on whether the test was successful
or not.
