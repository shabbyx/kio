Test description
================

This program tests `kputs`.  It creates `TEXT_FILE` opened in `"wp"`.  This
file name is defined in `test_info.h`.

The kernel opens the file and sends `TEXT_NUM_LINES` lines cycling from
`TEXT_RANGE_BEGIN` through `TEXT_RANGE_END` as `sprintf`ed.  It then proceeds
to send `TEXT_NUM_NON_LINES` strings cycling in the same range and truncated
to `TEXT_NON_LINE_LEN` characters.  These values are defined in `test_info.h`.

In the end, it will write EOKF and expect a write to the file to return with a failure.

Correspondingly, the user space program starts reading `TEXT_NUM_LINES` lines
from the file, expecting strings in proper order.  It then proceeds to read
`TEXT_NUM_NON_LINES` strings of length `TEXT_NON_LINE_LEN` expecting them in
proper order.

The user space program will return `EXIT_SUCCESS` if all operations were
successful and `EXIT_FAILURE` otherwise.  The kernel module will log
"kputs -- passed" or "kputs -- failed" based on whether the test was successful
or not.
