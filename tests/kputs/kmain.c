/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <kio.h>
#include "test_info.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Shahbaz Youssefi");

static struct task_struct *test_task;
static KFILE *pout;

static int write_and_check(KFILE *out, unsigned int count, unsigned int count2,
		unsigned int len, unsigned int begin, unsigned int end)
{
	unsigned int i;
	unsigned int n = begin;
	char str[20];
	for (i = 0; i < count; ++i)
	{
		sprintf(str, "%u\n", n);
		if (kputs(str, out) == EOKF)
			goto exit_bad_write;
		if (n == end)
			n = begin;
		else
			++n;
	}
	n = begin;
	for (i = 0; i < count2; ++i)
	{
		sprintf(str, "%u", n);
		str[len] = '\0';
		if (kputs(str, out) == EOKF)
			goto exit_bad_write;
		if (n == end)
			n = begin;
		else
			++n;
	}
	keouf(out);
	if (kputs("test\n", out) != EOKF)
		goto exit_bad_eouf;
	return 0;
exit_bad_write:
	printk(KERN_INFO "failed to write all data (at %uth data)\n", i);
	goto exit_fail;
exit_bad_eouf:
	printk(KERN_INFO "failed to stop writing after all data are finished\n");
	goto exit_fail;
exit_fail:
	return -1;
}

static int test_thread(void *arg)
{
	pout = kopen(TEXT_FILE, "wp");
	if (pout == NULL)
		goto exit_no_file;

	if (write_and_check(pout, TEXT_NUM_LINES, TEXT_NUM_NON_LINES, TEXT_NON_LINE_LEN,
				TEXT_RANGE_BEGIN, TEXT_RANGE_END))
		goto exit_fail;

	printk(KERN_INFO "kputs -- passed\n");
	goto exit_normal;
exit_no_file:
	printk(KERN_INFO "failed to open file\n");
	goto exit_fail;
exit_fail:
	printk(KERN_INFO "kputs -- failed\n");
exit_normal:
	if (pout)
		kclose(pout);
	pout = NULL;
	do_exit(0);
	return 0;
}

static int __init test_init(void)
{
	if ((test_task = kthread_run(test_thread, NULL, "kputs_test")) == ERR_PTR(-ENOMEM))
	{
		test_task = NULL;
		printk(KERN_INFO "initializing test... failed\n");
		return -ENOMEM;
	}
	printk(KERN_INFO "kputs test loaded\n");
	return 0;
}

static void __exit test_exit(void)
{
	if (pout)
		kclose(pout);
	printk(KERN_INFO "kputs test exited\n");
}

module_init(test_init);
module_exit(test_exit);
