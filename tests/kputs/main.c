/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "test_info.h"

static int input(FILE *f, unsigned int count, unsigned int count2, unsigned int len,
		unsigned int begin, unsigned int end)
{
	unsigned int i;
	unsigned int expected = begin;
	char str[100];
	char stre[20];
	for (i = 0; i < count; ++i)
	{
		if (fgets(str, 100, f) == NULL)
			goto exit_bad_fgets;
		sprintf(stre, "%u\n", expected);
		if (strcmp(str, stre) != 0)
			goto exit_mismatch;
		if (expected == end)
			expected = begin;
		else
			++expected;
	}
	expected = begin;
	for (i = 0; i < count2; ++i)
	{
		if (fgets(str, len + 1, f) == NULL)
			goto exit_bad_fgets;
		sprintf(stre, "%u", expected);
		stre[len] = '\0';
		if (strcmp(str, stre) != 0)
			goto exit_mismatch;
		if (expected == end)
			expected = begin;
		else
			++expected;
	}
	if (fgets(str, 100, f) != NULL)
		goto exit_bad_eof;
	return 0;
exit_bad_fgets:
	perror("error reading data");
	goto exit_fail;
exit_mismatch:
	fprintf(stderr, "failed to match with what was expected (at %uth data)", i);
	goto exit_fail;
exit_bad_eof:
	perror("error detecting eof after data were supposed to finish");
	goto exit_fail;
exit_fail:
	return -1;
}

int main(void)
{
	FILE *fin = NULL;
	fin = fopen("/proc/"TEXT_FILE, "r");
	if (fin == NULL)
		goto exit_no_file;
	if (input(fin, TEXT_NUM_LINES, TEXT_NUM_NON_LINES, TEXT_NON_LINE_LEN,
		TEXT_RANGE_BEGIN, TEXT_RANGE_END))
		goto exit_fail;
	if (fclose(fin))
	{
		fin = NULL;
		goto exit_bad_fclose;
	}
	return EXIT_SUCCESS;
exit_no_file:
	perror("error opening file");
	goto exit_fail;
exit_bad_fclose:
	perror("error closing file");
	goto exit_fail;
exit_fail:
	if (fin)
		fclose(fin);
	return EXIT_FAILURE;
}
