/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEST_INFO_H
#define TEST_INFO_H

#define TEXT_FILE "kputs_test"

#define TEXT_NUM_LINES 1000000
#define TEXT_NUM_NON_LINES 1000000
#define TEXT_NON_LINE_LEN 5
#define TEXT_RANGE_BEGIN 27169
#define TEXT_RANGE_END 123456

#endif
