kio
===

Introduction
------------

This kernel module, dubbed `kio`, brings C's standard `stdio` functions in kernel space.  For the most part, the
semantics of the functions are aligned with those of the standard.  Some functions however, e.g. `fopen`, need to
be modified for the kernel's needs.

To have large output from the kernel, there is already a wonderful `seq_file` feature.  `kio` approaches the problem
differently.  First of all, `kio` also supports multi-page inputs while `seq_file` is targeted only at output.
Second, `kio` lets the programmer output the same way he would in a normal program while `seq_file` assumes the
output would be a list of some `struct`.  Even though `seq_file`s _are_ more convenient when outputting similar data,
it can easily get ugly if they are not.

Motivation
----------

The reason I am writing this is twofold:

1. I simply have multiple modules that read large amount of initialization data from user-space.  I don't want to
   repeat myself.
2. The `/proc`, `/sys` and `/dev` file systems all do similar things.  However, they are all designed with a very
   low level file access mechanism and each one is different from the other.  What's worse is the fact that searching
   for information on how to work with them is hard on the internet, both because kernel programmers are not many and
   these system have changed over time and a lot of information on the internet is outdated.

This module, in the end, is supposed to support all (virtual) file systems available to a kernel module.  However, I
don't have the expertise in some of them yet, and I'm not sure if it is at all possible.  Nevertheless, I am starting
with providing generic multi-page read and write operations and use them with `/proc` files.  Using other file systems
would then be a matter of properly opening and closing the files and communicating the data with those generic
functions.

Build
-----

    $ autoreconf -i            # generate configure script
    $ mkdir build && cd build
    $ ../configure
    $ make
    $ sudo make install

To perform the tests, write:

    $ cd build
    $ make check

Make sure to check the kernel logs for possible messages.

Specifications
--------------

The name of the functions are the same as that of the standard, except their initial `f` is replaced with `k`.  Also,
some functions where the `f` is missing, a `k` is inserted, such as `kungetc`.  The `FILE` type is replaced by
`KFILE` (`KILE` was tempting, but no).  Functions that work with `stdin` and `stdout` don't exist anymore, simply
because these files don't exist.  However, if this module worked its way into the kernel itself and it was decided
to create `stdin`, `stdout` and `stderr` files for every module somewhere, they can be added.

Due to the nature of `kio`, file seeking is not planned to be implemented.

In this section, the specification of `kio` is written.  Later, I may add
[DocThis!](https://gitlab.com/ShabbyX/DocThis) documentation.

Additionally, the following require further debate and understanding:

- What happens to the data if a `/proc` file is deleted while a user program is reading it?
  * If the data is kept until the user picks it up, then change `kclose` and other places saying
    data will be flushed (instead of current "data will be discarded")
  * If the data is destroyed, set EOKF and find a way to make sure the user has picked the data up.
    Alternatively, set EOKF with `kflush` (or create a new function) and hope the user can pick it up
    before `kclose`.
- How to handle multiple users reading from the kernel?  With user write, we can assume it can be done mutually.
  With user read, even though we can say it is done mutually, in reality every device providing read operation
  provides it to multiple users.  The difficult part is how to distinguish which user is the current `fputc`
  writing to!
  * Solution: introduce two functions `kreadfunc` and `kwritefunc`.  They provide a callback to a function that
    reads from or writes to KFILE (`kreadfunc` is probably not so useful, but is there for symmetry).  Upon
    opening of the file by the user, if these callbacks are set, then create another `KFILE` (changing private data
    of `file_operations`) and add it to the list of user openings in the original `KFILE`.  Create a thread calling
    the appropriate callback.

    In this case also, `kgetc` and `kputc` on the original `KFILE` should fail.  If the callbacks are not provided,
    then the behavior is as it is now.  That is, only one user can open the file at a time.

Note: This section is formatted similar to section 7.21 (Input/Output `<stdio.h>`) of the C11 standard.  The item
numbers are the same as the corresponding ones in the standard so that they can be easily compared.  Missing
numbers indicate parts of the standard that are not included.  Numbers not in the standard are extensions.  It is
important however to know that some functions may behave slightly differently from the standard either for better
safety or just for being more tailored to the environment, which is the Linux kernel.  One such example is
defined behavior in case of error, instead of undefined, unspecified or implementation-defined behavior.

Please refer to the specification which is written _here_ rather than the standard.

Features are marked with ![yes](doc/yes.jpg) for implemented,
![in progress](doc/in_progress.jpg) for being implemented and
![no](doc/no.jpg) for not yet implemented.

### Input/Output `<kio.h>`

### 1 Introduction

![yes](doc/yes.jpg)
**1** The header `<kio.h>` defines several macros, and declares three types and many
functions for performing input and output.

![yes](doc/yes.jpg)
**2** The types declared are `size_t` (taken from kernel headers);

`KFILE`

which is an object type capable of recording all the information needed to control a
stream, including its file position indicator, a pointer to its associated buffer (if any), an
error indicator that records whether a read/write error has occurred, and an end-of-file
indicator that records whether the end of the file has been reached.

![yes](doc/yes.jpg)
**3** The macros are `NULL` (taken from kernel headers);

`EOKF`

which expands to an integer constant expression, with type int and a negative value, that
is returned by several functions to indicate end-of-file, that is, no more input from a
stream.

![yes](doc/yes.jpg)
**5** The input/output functions are given the following collective terms:

- The byte input/output functions - those functions described in this subclause that
perform input/output: `kgetc`, `kgets`, `kprintf`, `kputc`, `kputs`, `kread`,
`kscanf`, `kwrite`, `kungetc`, `vkprintf` and `vkscanf`.

### 2 Streams

![yes](doc/yes.jpg)
**1** Input and output, whether to or from `/proc`, `/sys`, `/dev` or any other (virtual)
file system available to the kernel module, are mapped into logical data streams, whose
properties are more uniform than their various inputs and outputs. Two forms of mapping are
supported, for _text streams_ and for _binary streams_.

![yes](doc/yes.jpg)
**2** A text stream is an ordered sequence of characters composed into lines, each line
consisting of zero or more characters plus a terminating new-line character. The last
line is required to have a terminating new-line character.

![yes](doc/yes.jpg)
**3** A binary stream is an ordered sequence of characters that can transparently record
internal data. Data read in from a binary stream shall compare equal to the data that were
earlier written out to that stream.

![yes](doc/yes.jpg)
**7** The streams are not protected against data races when multiple threads of execution
access them.

### 3 Files

![yes](doc/yes.jpg)
**1** A stream is associated with an external file by opening a file, which may involve
creating a new file. The file position indicator is maintained by subsequent reads and writes
to facilitate an orderly progression through the file.

![no](doc/no.jpg)
**2** Files never truncate.

![yes](doc/yes.jpg)
**3** The streams are fully buffered, i.e. characters are intended to be transmitted to or
from the other end of the stream (a user space application) as a block when a buffer is filled.

![no](doc/no.jpg)
**4** A file may be disassociated from a controlling stream by closing the file. Any unwritten
buffer contents are discarded and read requests blocked on input are returned with failure.
Operations on a closed `KFILE` object will fail.

![yes](doc/yes.jpg)
**5** Streams may not automatically close when the owning module is removed from the kernel.

![yes](doc/yes.jpg)
**6** The address of the `KFILE` object used to control a stream is insignificant. However,
a copy of a `KFILE` object may not serve in place of the original.

![yes](doc/yes.jpg)
**8** Functions that open files require a file name, which is a string. The rules for composing
valid file names are defined by the hosting Linux kernel. The same file cannot be simultaneously opened.

![yes](doc/yes.jpg)
**11** The byte input functions read characters from the stream as if by successive calls to
the `kgetc` function. All such functions block until enough data is available, or if there is
an error, or if the file is closed.

![yes](doc/yes.jpg)
**12** The byte output functions write characters to the stream as if by successive calls to
the `kputc` function. All such functions block until data is read by the other end of the stream
if the buffer is full, or if there is an error, or if the file is closed.

### 5 File access functions

#### ![in\_progress](doc/in_progress.jpg) 5.1 The `kclose` function

##### Synopsis

**1**

    #include <kio.h>
    void kclose(KFILE *stream);

##### Description

**2** A call to the `kclose` function causes the stream pointed to by `stream` to be closed.
Any unwritten or unread buffered data are discarded. The stream is disassociated from
the file.

##### Returns

**3** The `kclose` function returns no value.

#### ![no](doc/no.jpg) 5.2 The `kflush` function

##### Synopsis

**1**

    #include <kio.h>
    void kflush(KFILE *stream);

##### Description

**2** If `stream` points to an output stream, the `kflush` function causes any unwritten data for
that stream to be delivered to the other end of the stream. Otherwise, the function doesn't
do anything. There is no guarantee that the other end of the stream actually reads the data.

##### Returns

**4** The `kflush` function returns no value.

#### ![in\_progress](doc/in_progress.jpg) 5.3 The `kopen` function

##### Synopsis

**1**

    #include <kio.h>
    KFILE *kopen(const char * restrict filename, const char * restrict mode);

##### Description

![no](doc/no.jpg)
**2** The `kopen` function opens the file whose name is the string pointed to by `filename`,
and associates a stream with it. Depending on which underlying subsystem is used, this file
is placed in a different directory. If the file name includes a path, i.e. includes directory
separation character `/`, the corresponding directories are created if possible.

**3** The argument `mode` points to a string. The string is made of the following characters
in any order. If conflicting modes are given, the last one is accepted.

- **File System Type** (mandatory): The type of the underlying file system. Only one of the following
  values must be chosen.
  * ![yes](doc/yes.jpg) `p`: Opens a file under `/proc`
  * ![no](doc/no.jpg) `s`: Opens a file under `/sys`
  * ![no](doc/no.jpg) `d`: Opens a file under `/dev`
- **Direction** (mandatory): The direction of data to or from the kernel. At least on of the
  following values must be chosen. Note that it is also possible to choose both if the file system
  accepts it.
  * ![yes](doc/yes.jpg) 'r': Opens a file in read mode
  * ![yes](doc/yes.jpg) 'w': Opens a file in write mode
- **Mode** (optional): Whether the file is in binary mode. At most one of the following values
  can be chosen. If none chosen, the file is opened in binary mode.
  * ![yes](doc/yes.jpg) 'b': Opens a file in binary mode

**4** Opening a file may fail if the file does not exist in certain file systems or cannot be created.

##### Returns

**9** The `kopen` function returns a pointer to the object controlling the stream. If the open
operation fails, `kopen` returns a null pointer.

#### ![yes](doc/yes.jpg) 5.7 The `keouf` function

##### Synopsis

**1**

    #include <kio.h>
    void keouf(KFILE *stream);

##### Description

**2** The `keouf` function flushes the write buffer and signals the other end of the stream that
`EOF` for this stream has been reached.

##### Returns

**3** The `keouf` function returns no value.

### 6 Formatted input/output functions

#### ![yes](doc/yes.jpg) 6.1 The `kprintf` function

##### Synopsis

**1**

    #include <kio.h>
    int kprintf(KFILE * restrict stream, const char * restrict format, ...);

##### Description

**2** The `kprintf` function writes output to the stream pointed to by `stream`, under control
of the string pointed to by `format` that specifies how subsequent arguments are converted for
output. If there are insufficient arguments for the format, the behavior is undefined. If the
format is exhausted while arguments remain, the excess arguments are evaluated (as always) but
are otherwise ignored. The `kprintf` function returns when the end of the format string is
encountered.

**3** The format shall be a basic character sequence. The format is composed of zero or more
directives: ordinary basic characters (not `%`), which are copied unchanged to the output stream;
and conversion specifications, each of which results in fetching zero or more subsequent arguments,
converting them, if applicable, according to the corresponding conversion specifier, and
then writing the result to the output stream.

**4** Each conversion specification is introduced by the character `%`. After the `%`,
the following appear in sequence:

- Zero or more _flags_ (in any order) that modify the meaning of the conversion
  specification.
- An optional minimum _field width_. If the converted value has fewer characters than the
  field width, it is padded with spaces (by default) on the left (or right, if the left
  adjustment flag, described later, has been given) to the field width. The field width
  takes the form of an asterisk * (described later) or a nonnegative decimal integer.
- An optional _precision_ that gives the minimum number of digits to appear for the `d`, `i`,
  `o`, `u`, `x`, and `X` conversions, or the maximum number of bytes to be written for
  `s` conversions. The precision takes the form of a period (`.`) followed either by an
  asterisk `*` (described later) or by an optional decimal integer; if only the period is
  specified, the precision is taken as zero. If a precision appears with any other
  conversion specifier, it is ignored.
- An optional _length modifier_ that specifies the size of the argument.
- A _conversion specifier_ character that specifies the type of conversion to be applied.

**5** As noted above, a field width, or precision, or both, may be indicated by an asterisk. In
this case, an `int` argument supplies the field width or precision. The arguments
specifying field width, or precision, or both, shall appear (in that order) before the
argument (if any) to be converted. A negative field width argument is taken as a `-` flag
followed by a positive field width. A negative precision argument is taken as if the
precision were omitted.

**6** The flag characters and their meanings are:

- ![yes](doc/yes.jpg)
  `-`: The result of the conversion is left-justified within the field. (It is right-justified if
  this flag is not specified.)
- ![yes](doc/yes.jpg)
  `+`: The result of a signed conversion always begins with a plus or minus sign. (It
  begins with a sign only when a negative value is converted if this flag is not specified.)
- ![yes](doc/yes.jpg)
  _space_: If the first character of a signed conversion is not a sign, or if a signed conversion
  results in no characters, a space is prefixed to the result. If the space and + flags
  both appear, the space flag is ignored.
- ![yes](doc/yes.jpg)
  `#`: The result is converted to an "alternative form". For `o` conversion, it increases
  the precision, if and only if necessary, to force the first digit of the result to be a
  zero. For `x` (or `X`) conversion, a nonzero result has `0x` (or `0X`) prefixed to it. For other
  conversions, this flag is ignored. If the value and precision are both 0, a single 0 is printed.
- ![yes](doc/yes.jpg)
  `0`: For `d`, `i`, `o`, `u`, `x` and `X` conversions, leading zeros
  (following any indication of sign or base) are used to pad to
  the field width rather than performing space padding. If the `0` and `-` flags both appear,
  the `0` flag is ignored. For `d`, `i`, `o`, `u`, `x`, and `X` conversions, if a precision is
  specified, the `0` flag is ignored. For other conversions, this flag is ignored.

**7** The length modifiers and their meanings are:

- ![yes](doc/yes.jpg)
  `hh`: Specifies that a following `d`, `i`, `o`, `u`, `x`, or `X` conversion specifier applies to a
  `signed char` or `unsigned char` argument (the argument will have been promoted according to the
  integer promotions, but its value shall be converted to `signed char` or `unsigned char` before
  printing); or that a following `n` conversion specifier applies to a pointer to a `signed char`
  argument.
- ![yes](doc/yes.jpg)
  `h`: Similar to `hh`, but with `short int` instead of `char`.
- ![yes](doc/yes.jpg)
  `l` (ell): Similar to `hh`, but with `long int` instead of `char`.
- ![yes](doc/yes.jpg)
  `ll` (ell-ell): Similar to `hh`, but with `long long int` instead of `char`.
- ![yes](doc/yes.jpg)
  `z`: Similar to `hh`, but with `size_t` instead of `unsigned char` and its corresponding signed
  integer type instead of `char`.

If a length modifier appears with any conversion specifier other than as specified above,
it is ignored.

**8** The conversion specifiers and their meanings are:

- ![yes](doc/yes.jpg)
  `d`, `i`: The `int` argument is converted to signed decimal in the style _[−]dddd_. The
  precision specifies the minimum number of digits to appear; if the value
  being converted can be represented in fewer digits, it is expanded with
  leading zeros. The default precision is 1. The result of converting a zero
  value with a precision of zero is no characters.
- ![yes](doc/yes.jpg)
  `o`, `u`, `x`, `X`: The `unsigned int` argument is converted to unsigned octal (`o`), unsigned
  decimal (`u`), or unsigned hexadecimal notation (`x` or `X`) in the style _dddd_; the
  letters `abcdef` are used for `x` conversion and the letters `ABCDEF` for `X`
  conversion. The precision specifies the minimum number of digits to appear with the same
  default and behavior as the `d` specifier.
- ![yes](doc/yes.jpg)
  `c`: The `int` argument is converted to an `unsigned char`, and the resulting character is written.
- ![yes](doc/yes.jpg)
  `s`: The argument shall be a pointer to the initial element of an array of character type.
  Characters from the array are written up to (but not including) the terminating null character.
  If the precision is specified, no more than that many bytes are written. If the precision is not
  specified or is greater than the size of the array, the array shall contain a null character.
- ![yes](doc/yes.jpg)
  `p`: The argument shall be a pointer to `void`. The value of the pointer is printed as if given
  an `x` specifier with the `#` flag.
- ![yes](doc/yes.jpg)
  `n`: The argument shall be a pointer to signed integer into which is _written_ the
  number of characters written to the output stream so far by this call to `kprintf`.
  No argument is converted, but one is consumed. If the conversion specification includes any flags,
  a field width, or a precision, they are ignored.
- ![yes](doc/yes.jpg)
  `%`: A `%` character is written. No argument is converted. The complete conversion specification
  shall be `%%`.

**9** If a conversion specification is invalid, the directive fails. If any argument is not the correct
type for the corresponding conversion specification, the behavior is undefined.

**10** In no case does a nonexistent or small field width cause truncation of a field; if the result
of a conversion is wider than the field width, the field is expanded to contain the conversion result.

##### Returns

**14** The `kprintf` function returns the number of characters transmitted, or a negative value
if an output error occurred.

#### ![yes](doc/yes.jpg) 6.2 The `kscanf` function

##### Synopsis

**1**

    #include <kio.h>
    int kscanf(KFILE * restrict stream, const char * restrict format, ...);

##### Description

**2** The `kscanf` function reads input from the stream pointed to by `stream`, under control
of the string pointed to by `format` that specifies the admissible input sequences and how
they are to be converted for assignment, using subsequent arguments as pointers to the
objects to receive the converted input. If there are insufficient arguments for the format,
the behavior is undefined. If the format is exhausted while arguments remain, the excess
arguments are evaluated (as always) but are otherwise ignored.

**3** The format shall be a basic character sequence. The format is composed of zero or more
directives: one or more white-space characters, an ordinary basic character (neither `%` nor a
white-space character), or a conversion specification. Each conversion specification is introduced
by the character `%`. After the `%`, the following appear in sequence:

- An optional assignment-suppressing character `*`.
- An optional decimal integer greater than zero that specifies the maximum field width
  (in characters).
- An optional _length modifier_ that specifies the size of the receiving object.
- A _conversion specifier_ character that specifies the type of conversion to be applied.

**4** The `kscanf` function executes each directive of the format in turn. When all directives
have been executed, or if a directive fails (as detailed below), the function returns.
Failures are described as input failures (due to the unavailability of input characters),
or matching failures (due to inappropriate input).

**5** A directive composed of white-space character(s) is executed by reading input up to the
first non-white-space character (which remains unread), or until no more characters can
be read. The directive never fails.

**6** A directive that is an ordinary basic character is executed by reading the next
characters of the stream. If any of those characters differ from the ones composing the
directive, the directive fails and the differing and subsequent characters remain unread.
Similarly, if end-of-file, or a read error prevents a character from being read, the directive fails.

**7** A directive that is a conversion specification defines a set of matching input sequences, as
described below for each specifier. A conversion specification is executed in the following steps:

**8** Input white-space characters (as specified by the `isspace` function) are skipped, unless
the specification includes a `[`, `c`, or `n` specifier.

**9** An input item is read from the stream, unless the specification includes an `n` specifier. An
input item is defined as the longest sequence of input characters which does not exceed
any specified field width and which is, or is a prefix of, a matching input sequence.
The first character, if any, after the input item remains unread. If the length of the input
item is zero, the execution of the directive fails; this condition is a matching failure unless
end-of-file, or a read error prevented input from the stream, in which case it is an input failure.

**10** Except in the case of a `%` specifier, the input item (or, in the case of a `%n` directive, the
count of input characters) is converted to a type appropriate to the conversion specifier. If
the input item is not a matching sequence, the execution of the directive fails: this
condition is a matching failure. Unless assignment suppression was indicated by a `*`, the
result of the conversion is placed in the object pointed to by the first argument following
the format argument that has not already received a conversion result. If this object
does not have an appropriate type, or if the result of the conversion cannot be represented
in the object, the behavior is undefined.

**11** The length modifiers and their meanings are:

- ![yes](doc/yes.jpg)
  `hh`: Specifies that a following `d`, `i`, `o`, `u`, `x`, `X`, or `n` conversion specifier applies
   to an argument with type pointer to `signed char` or `unsigned char`.
- ![yes](doc/yes.jpg)
  `h`: Similar to `hh`, but with `short int` instead of `char`.
- ![yes](doc/yes.jpg)
  `l` (ell): Similar to `hh`, but with `long int` instead of `char`.
- ![yes](doc/yes.jpg)
  `ll` (ell-ell): Similar to `hh`, but with `long long int` instead of `char`.
- ![yes](doc/yes.jpg)
  `z`: Similar to `hh`, but with `size_t` instead of `unsigned char` and its corresponding signed
  integer type instead of `char`.

If a length modifier appears with any conversion specifier other than as specified above,
it is ignored.

**12** The conversion specifiers and their meanings are:

- ![yes](doc/yes.jpg)
  `d`: Matches an optionally signed decimal integer.
  The corresponding argument shall be a pointer to signed integer.
- ![yes](doc/yes.jpg)
  `i`: Matches an optionally signed integer, whose format is the same as either of
  `d`, `o` or `x` specifiers and will perform the read in the corresponding base.
  The corresponding argument shall be a pointer to signed integer.
- ![yes](doc/yes.jpg)
  `o`: Matches an optionally signed octal integer. After the sign, the number may be
  prefixed with a `0`. The corresponding argument shall be a pointer to unsigned integer.
- ![yes](doc/yes.jpg)
  `u`: Matches an optionally signed decimal integer.
  The corresponding argument shall be a pointer to unsigned integer.
- ![yes](doc/yes.jpg)
  `x`, `X`: Matches an optionally signed hexadecimal integer. After the sign, the number may be
  prefixed with a `0x`. The corresponding argument shall be a pointer to unsigned integer.
- ![yes](doc/yes.jpg)
  `c`: Matches a sequence of characters of exactly the number specified by the field
  width (1 if no field width is present in the directive). The corresponding argument shall
  be a pointer to the initial element of a character array large enough to accept the
  sequence. No null character is added.
- ![yes](doc/yes.jpg)
  `s`: Matches a sequence of non-white-space characters. The corresponding argument shall be a
  pointer to the initial element of a character array large enough to accept the
  sequence and a terminating null character, which will be added automatically.
- ![yes](doc/yes.jpg)
  `[`: Matches a nonempty sequence of characters from a set of expected characters (the scanset).
  The corresponding argument shall be a pointer to the initial element of a character array large
  enough to accept the sequence and a terminating null character, which will be added automatically.

  The conversion specifier includes all subsequent characters in the `format`
  string, up to and including the matching right bracket (`]`). The characters
  between the brackets (the _scanlist_) compose the scanset, unless the character
  after the left bracket is a circumflex (`^`), in which case the scanset contains all
  characters that do not appear in the scanlist between the circumflex and the
  right bracket. If the conversion specifier begins with `[]` or `[^]`, the right
  bracket character is in the scanlist and the next following right bracket
  character is the matching right bracket that ends the specification; otherwise
  the first following right bracket character is the one that ends the
  specification. If a `-` character is in the scanlist and is not the first, nor the
  second where the first character is a `^`, nor the last character, all characters
  between the two characters surrounding the `-` will be in the scanlist. Computing
  the characters between two characters is implementation defined.
- ![yes](doc/yes.jpg)
  `p`: Matches a hexadecimal number, which should be the same as the output that may
  be produced by the `%p` conversion of the `kprintf` function. The corresponding argument
  shall be a pointer to a pointer to `void`. The input item is converted to a pointer value
  in an implementation-defined manner. If the input item is a value converted earlier
  during the same program execution, the pointer that results shall compare
  equal to that value; otherwise the behavior of the `%p` conversion is undefined.
- ![yes](doc/yes.jpg)
  `n`: No input is consumed. The corresponding argument shall be a pointer to
  signed integer into which is to be written the number of characters read from
  the input stream so far by this call to the `kscanf` function. Execution of a
  `%n` directive does not increment the assignment count returned at the
  completion of execution of the `kscanf` function. No argument is converted,
  but one is consumed. If the conversion specification includes an assignment-suppressing
  character or a field width, the function returns with a failure.
- ![yes](doc/yes.jpg)
  `%`: Matches a single `%` character; no conversion or assignment occurs. The
  complete conversion specification shall be `%%`.

**13** If a conversion specification is invalid, the function returns as if the end of the
format string is reached. If any argument is not the correct type for the corresponding conversion
specification, the behavior is undefined.

**15** Trailing white space (including new-line characters) is left unread unless matched by a
directive. The success of literal matches and suppressed assignments is not directly
determinable other than via the `%n` directive.

##### Returns

**16** The `kscanf` function returns the value of the macro `EOKF` if an input failure occurs
before the first conversion (if any) has completed. Otherwise, the function returns the
number of input items assigned, which can be fewer than provided for, or even zero, in
the event of an early matching failure.

#### ![yes](doc/yes.jpg) 6.8 The `vkprintf` function

##### Synopsis

**1**

    #include <kio.h>
    int vkprintf(KFILE * restrict stream, const char * restrict format, va_list arg);

##### Description

**2** The `vkprintf` function is equivalent to `kprintf`, with the variable argument list
replaced by `arg`, which shall have been initialized by the `va_start` macro (and
possibly subsequent `va_arg` calls). The `vkprintf` function does not invoke the
`va_end` macro.

##### Returns

**3** The `vkprintf` function returns the same value as the equivalent `kprintf`.

#### ![yes](doc/yes.jpg) 6.9 The `vkscanf` function

##### Synopsis

**1**

    #include <kio.h>
    int vkscanf(KFILE * restrict stream, const char * restrict format, va_list arg);

##### Description

**2** The `vkscanf` function is equivalent to `kscanf`, with the variable argument list
replaced by `arg`, which shall have been initialized by the `va_start` macro (and
possibly subsequent `va_arg` calls). The `vkscanf` function does not invoke the
`va_end` macro.

##### Returns

**3** The `vkscanf` function returns the same value as the equivalent `kscanf`.

### 7 Character input/output functions

#### ![yes](doc/yes.jpg) 7.1 The `kgetc` function

##### Synopsis

**1**

    #include <kio.h>
    int kgetc(KFILE * stream);

##### Description

**2** If the end-of-file indicator for the input stream pointed to by `stream` is not set and a
next character is present, the `kgetc` function obtains that character as an `unsigned
char` converted to an `int` and advances the associated file position indicator for the
stream.

##### Returns

**3** If the end-of-file indicator for the stream is set, or if the stream is at end-of-file, the end-
of-file indicator for the stream is set and the `kgetc` function returns `EOKF`. Otherwise, the
`kgetc` function returns the next character from the input stream pointed to by stream.
If a read error occurs, the error indicator for the stream is set and the `kgetc` function
returns `EOKF`.

#### ![yes](doc/yes.jpg) 7.2 The `kgets` function

##### Synopsis

**1**

    #include <kio.h>
    char *kgets(char * restrict s, int n, KFILE * restrict stream);

##### Description

**2** The `kgets` function reads at most one less than the number of characters specified by `n`
from the stream pointed to by `stream` into the array pointed to by `s`. No additional
characters are read after a new-line character (which is retained) or after end-of-file. A
nul character is written immediately after the last character read into the array.

##### Returns

**3** The `kgets` function returns `s` if successful. If end-of-file is encountered and no
characters have been read into the array, the contents of the array remain unchanged and a
null pointer is returned. If a read error occurs during the operation, the array contents are
indeterminate and a null pointer is returned.

#### ![yes](doc/yes.jpg) 7.3 The `kputc` function

##### Synopsis

**1**

    #include <kio.h>
    int kputc(int c, KFILE *stream);

##### Description

**2** The `kputc` function writes the character specified by `c` (converted to an `unsigned
char`) to the output stream pointed to by stream, at the position indicated by the
associated file position indicator for the stream, and advances the indicator
appropriately.

##### Returns

**3** The `kputc` function returns the character written. If a write error occurs, the error
indicator for the stream is set and `kputc` returns `EOKF`.

#### ![yes](doc/yes.jpg) 7.4 The `kputs` function

##### Synopsis

**1**

    #include <kio.h>
    int kputs(char * restrict s, KFILE * restrict stream);

##### Description

**2** The kputs` function writes the string pointed to by `s` to the stream pointed to by
`stream`. The terminating null character is not written.

##### Returns

**3** The `kputs` function returns `EOKF` if a write error occurs; otherwise it returns the
number of characters written.  If `keouf` has been called on the stream, `EOKF` shall be returned.

#### ![yes](doc/yes.jpg) 7.10 The `kungetc` function

##### Synopsis

**1**

    #include <kio.h>
    int kungetc(int c, KFILE *stream);

##### Description

**2** The `kungetc` function pushes the character specified by `c` (converted to an `unsigned
char`) back onto the input stream pointed to by `stream`. Pushed-back characters will be
returned by subsequent reads on that stream in the reverse order of their pushing.
The external storage corresponding to the stream is unchanged.

**3** One character of pushback is guaranteed. If the `kungetc` function is called too many
times on the same stream without an intervening read, the operation may fail.

**4** If the value of `c` equals that of the macro `EOKF`, the operation fails and the input stream is
unchanged.

**5** A successful call to the `kungetc` function clears the end-of-file indicator for the stream.
The value of the file position indicator for the stream after reading or discarding all
pushed-back characters shall be the same as it was before the characters were pushed
back.

##### Returns

**6** The `kungetc` function returns the character pushed back after conversion, or `EOKF` if the
operation fails.

### 8 Direct input/output functions

#### ![yes](doc/yes.jpg) 8.1 The `kread` function

##### Synopsis

**1**

    #include <kio.h>
    size_t kread(void * restrict ptr, size_t size, size_t nmemb, KFILE * restrict stream);

##### Description

**2** The `kread` function reads, into the array pointed to by `ptr`, up to `nmemb` elements
whose size is specified by `size`, from the stream pointed to by `stream`. For each
object, `size` calls are made to the `kgetc` function and the results stored, in the order
read, in an array of `unsigned char` exactly overlaying the object. The file position
indicator for the stream is advanced by the number of characters successfully
read. If an error occurs, the resulting value of the file position indicator for the stream is
indeterminate. If a partial element is read, its value is indeterminate.

##### Returns

**3** The `kread` function returns the number of elements successfully read, which may be
less than `nmemb` if a read error or end-of-file is encountered. If `size` or `nmemb` is zero,
`kread` returns zero and the contents of the array and the state of the stream remain
unchanged.

#### ![yes](doc/yes.jpg) 8.2 The `kwrite` function

##### Synopsis

**1**

    #include <kio.h>
    size_t kwrite(const void * restrict ptr, size_t size, size_t nmemb, KFILE * restrict stream);

##### Description

**2** The `kwrite` function writes, from the array pointed to by `ptr`, up to `nmemb` elements
whose `size` is specified by size, to the stream pointed to by `stream`. For each object,
`size` calls are made to the `kputc` function, taking the values (in order) from an array of
`unsigned char` exactly overlaying the object. The file position indicator for the
stream is advanced by the number of characters successfully written. If an
error occurs, the resulting value of the file position indicator for the stream is
indeterminate.

##### Returns

**3** The `kwrite` function returns the number of elements successfully written, which will be
less than `nmemb` only if a write error is encountered. If `size` or `nmemb` is zero,
`kwrite` returns zero and the state of the stream remains unchanged.

### 10 Error-handling functions

#### ![yes](doc/yes.jpg) 10.1 The `kclearerr` function

##### Synopsis

**1**

    #include <kio.h>
    void kclearerr(KFILE *stream);

##### Description

**2** The `kclearerr` function clears the end-of-file and error indicators for the stream pointed
to by `stream`.

##### Returns

**3** The `kclearerr` function returns no value.

#### ![yes](doc/yes.jpg) 10.2 The `keokf` function

##### Synopsis

**1**

    #include <kio.h>
    int keokf(KFILE *stream);

##### Description

**2** The `keokf` function tests the end-of-file indicator for the stream pointed to by `stream`.

##### Returns

**3** The `keokf` function returns nonzero if and only if the end-of-file indicator is set for
`stream`.

#### ![yes](doc/yes.jpg) 10.3 The `kerror` function

##### Synopsis

**1**

    #include <kio.h>
    int kerror(KFILE *stream);

##### Description

**2** The `kerror` function tests the error indicator for the stream pointed to by `stream`.

##### Returns

**3** The `kerror` function returns nonzero if and only if the either of the read or write error
indicators is set for `stream`.

#### ![yes](doc/yes.jpg) 10.5 The `krerror` function

##### Synopsis

**1**

    #include <kio.h>
    int krerror(KFILE *stream);

##### Description

**2** The `krerror` function tests the read error indicator for the stream pointed to by `stream`.

##### Returns

**3** The `krerror` function returns nonzero if and only if the read error indicator is set for `stream`.

#### ![yes](doc/yes.jpg) 10.6 The `kwerror` function

##### Synopsis

**1**

    #include <kio.h>
    int kwerror(KFILE *stream);

##### Description

**2** The `kwerror` function tests the write error indicator for the stream pointed to by `stream`.

##### Returns

**3** The `kwerror` function returns nonzero if and only if the write error indicator is set for `stream`.
