/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "internal.h"

size_t kread(void * KIO_RESTRICT ptr, size_t size, size_t nmemb, KFILE * KIO_RESTRICT stream)
{
	int i, j, cur = 0;

	if (size == 0 || nmemb == 0)
		return 0;

	for (i = 0; i < nmemb; ++i)
		for (j = 0; j < size; ++j)
		{
			int c = kgetc(stream);
			if (c == EOKF)
				goto exit_eokf;

			((unsigned char *)ptr)[cur++] = (unsigned char)c;
		}
exit_eokf:
	return i;
}
EXPORT_SYMBOL(kread);

size_t kwrite(const void * KIO_RESTRICT ptr, size_t size, size_t nmemb, KFILE * KIO_RESTRICT stream)
{
	int i, j, cur = 0;

	if (size == 0 || nmemb == 0)
		return 0;

	for (i = 0; i < nmemb; ++i)
		for (j = 0; j < size; ++j)
			if (kputc(((unsigned char *)ptr)[cur++], stream) == EOKF)
				goto exit_eokf;
exit_eokf:
	return i;
}
EXPORT_SYMBOL(kwrite);
