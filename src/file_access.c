/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/vmalloc.h>
#include "procfs.h"
#include "sysfs.h"
#include "devfs.h"

void kio_unblock(KFILE *kf)
{
	kf->eokf = true;
	if (kf->read_mode)
		up(&kf->input);
	if (kf->write_mode)
		up(&kf->output);
	kf->uopen = false;
}

void kclose(KFILE *stream)
{
	if (stream == NULL)
		return;
	/* kflush(stream); */
	switch (stream->type)
	{
	case KIO_PROCFS:
		kio_procfs_close(&stream->fs.proc);
		break;
	case KIO_SYSFS:
		break;
	case KIO_DEVFS:
		break;
	default:
		break;
	}

	kio_unblock(stream);

	if (stream->read_buffer_mem_size != 0)	/* then it's a vmalloced memory */
		vfree(stream->read_buffer);
	if (stream->write_buffer_mem_size != 0)	/* likewise */
		vfree(stream->write_buffer);
	vfree(stream);
}
EXPORT_SYMBOL(kclose);

/*void kflush(KFILE *stream);
EXPORT_SYMBOL(kflush);*/

void keouf(KFILE *stream)
{
	stream->eouf = true;
	up(&stream->write);
}
EXPORT_SYMBOL(keouf);

KFILE *kopen(const char * KIO_RESTRICT filename, const char * KIO_RESTRICT mode)
{
	KFILE *file = NULL;
	bool for_read = false;
	bool for_write = false;
	enum kio_fs_type fs_type = KIO_INVALID;
	bool binary = false;
	const char *m;

	/* parse mode */
	for (m = mode; *m; ++m)
		switch (*m)
		{
		case 'p':
			fs_type = KIO_PROCFS;
			break;
		case 's':
			fs_type = KIO_SYSFS;
			break;
		case 'd':
			fs_type = KIO_DEVFS;
			break;
		case 'r':
			for_read = true;
			break;
		case 'w':
			for_write = true;
			break;
		case 'b':
			binary = true;
			break;
		default:
			break;
		}
	if ((!for_read && !for_write) || fs_type == KIO_INVALID)
		goto exit_bad_mode;

	/* initialize file object */
	file = vmalloc(sizeof(*file));
	if (file == NULL)
		goto exit_no_mem;
	*file = (KFILE) {
		.open = true,
		.read_mode = for_read,
		.write_mode = for_write,
		.binary = binary,
		.type = fs_type
	};
	if (for_read)
	{
		sema_init(&file->input, 0);	/* kgetc must initially wait */
		sema_init(&file->read, 1);	/* reader can start reading immediately */
	}
	if (for_write)
	{
		sema_init(&file->output, 0);	/* kputc must initially wait */
		sema_init(&file->write, 0);	/* writer must wait for data to be written */
	}

	/* request respecting file system to open the file */
	switch (fs_type)
	{
	case KIO_PROCFS:
		file->fs.proc.file = file;
		if (kio_procfs_open(&file->fs.proc, filename, for_read, for_write))
			goto exit_cant_open;
		break;
	case KIO_SYSFS:
		file->fs.sys.file = file;
		goto exit_no_support;
	case KIO_DEVFS:
		file->fs.dev.file = file;
		goto exit_no_support;
	default:
		goto exit_internal;
	}
	return file;

exit_bad_mode:
	DBG("bad mode \"%s\"", mode);
	goto exit_fail;
exit_no_mem:
	DBG("out of memory");
	goto exit_fail;
exit_no_support:
	LOG("mode not implemented yet: %s", fs_type == KIO_PROCFS?"procfs":
					fs_type == KIO_SYSFS?"sysfs":
					fs_type == KIO_DEVFS?"devfs":
					"<internal error>");
	goto exit_fail;
exit_internal:
	LOG("internal error: caught missing file-system in kopen mode too late");
	goto exit_fail;
exit_cant_open:
	DBG("could not open file: \"%s\"", filename);
	goto exit_fail;
exit_fail:
	vfree(file);
	return NULL;
}
EXPORT_SYMBOL(kopen);
