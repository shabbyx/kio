/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/init.h>
#include <linux/module.h>
#include "internal.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Shahbaz Youssefi <ShabbyX@gmail.com>");
MODULE_DESCRIPTION("kio, the equivalent of stdio in kernel");
MODULE_VERSION(PACKAGE_VERSION);

static int __init kio_init(void)
{
	DBG("kio loaded");
	return 0;
}

static void __exit kio_exit(void)
{
	DBG("kio exited");
}

module_init(kio_init);
module_exit(kio_exit);
