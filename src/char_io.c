/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "internal.h"

int kgetc(KFILE * stream)
{
	/*
	 * Note: in Linux, everything is Ok.  If ever this was to be ported to
	 * a system with a different line ending, this whole thing should be
	 * put in a do-while loop (which may work 1 or 2 times).
	 *
	 * For example, if line endings in the said system is '\r\n', then
	 * the while loop will simply loop twice if the file is not opened in
	 * binary mode and if the currently read character is '\r'.
	 *
	 * In unix-like OSes, the line endings are already '\n', so there is
	 * no need for any conversions
	 */

	int c = EOKF;

	if (!stream->read_mode)
		return EOKF;

	/* if there is a character that was `kungetc`ed, return that */
	if (stream->ungot > 0)
	{
		--stream->ungot;
		return stream->ungot_chars[stream->ungot];
	}

	/*
	 * if there was an error or end of file has been reached
	 * and no more data is left, return immediately
	 */
	if (krerror(stream) || (keokf(stream) &&
		stream->read_index >= stream->read_buffer_size))
		return EOKF;

	/* otherwise wait until more input is available */
	if (down_interruptible(&stream->input))
		goto exit_error;
	/* fix possible duplicate `up` due to user file close */
	if (down_trylock(&stream->input) == 0)
		DBG("fixed stream->input that was uped twice");

	/* read as long as there is more data if not read_error */
	if (!krerror(stream) && stream->read_index < stream->read_buffer_size)
		c = (unsigned char)stream->read_buffer[stream->read_index++];

	if (stream->read_index < stream->read_buffer_size)
		up(&stream->input);	/* more input is still available */
	else if (!keokf(stream))
		up(&stream->read);	/* signal reader to get more data */

	return c;
exit_error:
	stream->read_error = true;
	return EOKF;
}
EXPORT_SYMBOL(kgetc);

char *kgets(char * KIO_RESTRICT s, int n, KFILE * KIO_RESTRICT stream)
{
	int i;
	for (i = 0; i < n - 1; ++i)
	{
		int c = kgetc(stream);
		if (c == EOKF)
			break;

		s[i] = (unsigned char)c;

		if (c == '\n')
		{
			++i;
			break;
		}
	}
	if (i == 0 || krerror(stream))
		return NULL;

	s[i] = '\0';
	return s;
}
EXPORT_SYMBOL(kgets);

int kputc(int c, KFILE *stream)
{
	/*
	 * Note: in Linux, everything is Ok.  If ever this was to be ported to
	 * a system with a different line ending, this whole thing should be
	 * put in a do-while loop (which may work 1 or 2 times).
	 *
	 * For example, if line endings in the said system is '\r\n', then
	 * the while loop will simply loop twice if the file is not opened in
	 * binary mode and if `c` is '\n', in which case first '\r' and then
	 * '\n' will be written.
	 *
	 * In unix-like OSes, the line endings are already '\n', so there is
	 * no need for any conversions
	 */

	if (!stream->write_mode)
		return EOKF;

	/* if there was an error or end of file (user or kernel) has been reached */
	if (kwerror(stream) || stream->eouf || keokf(stream))
		return EOKF;

	/* otherwise wait until more output space is available */
	if (down_interruptible(&stream->output))
		goto exit_error;
	/* fix possible duplicate `up` due to user file close */
	if (down_trylock(&stream->output) == 0)
		DBG("fixed stream->output that was uped twice");

	/* write as long as there is more space if not write_error */
	if (!kwerror(stream) && stream->write_index < stream->write_buffer_size)
		stream->write_buffer[stream->write_index++] = (unsigned char)c;

	if (stream->write_index < stream->write_buffer_size)
		up(&stream->output);	/* more space is still available */
	else if (!keokf(stream) && !stream->eouf)
		up(&stream->write);	/* signal writer to send data and get more space */

	return c;	
exit_error:
	stream->write_error = true;
	return EOKF;
}
EXPORT_SYMBOL(kputc);

int kputs(char * KIO_RESTRICT s, KFILE * KIO_RESTRICT stream)
{
	int i;

	if (keokf(stream) || stream->eouf)
		return EOKF;

	for (i = 0; s[i] != '\0'; ++i)
		if (kputc(s[i], stream) == EOKF)
			break;

	if (kwerror(stream))
		return EOKF;

	return i;
}
EXPORT_SYMBOL(kputs);

int kungetc(int c, KFILE *stream)
{
	if (stream->ungot >= KIO_KUNGETC_CHARS)
		return EOKF;

	stream->ungot_chars[stream->ungot++] = (unsigned char)c;

	return stream->ungot_chars[stream->ungot - 1];
}
EXPORT_SYMBOL(kungetc);
