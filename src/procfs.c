/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/vmalloc.h>
#include <asm/uaccess.h>
#include "procfs.h"

/*
 * This function enlarges buffer up to size.  If size is smaller than
 * mem_size, it leaves the buffer be.
 */
static int allocate_buffer(char **buf, size_t *mem_size, size_t size)
{
	if (*buf == NULL || *mem_size < size)
	{
		vfree(*buf);
		*buf = vmalloc(size * sizeof(**buf));
		*mem_size = size;
	}
	return (*buf == NULL);	/* 0 if successful */
}

/* users read, so used for write mode */
static ssize_t procfs_cb_read(struct file *file, char __user *buf,
		size_t count, loff_t *ppos)
{
	struct proc_dir_entry *pde = PDE(file->f_path.dentry->d_inode);
	KFILE *kf = pde->data;

	/* TODO: how to tell EOF? */
	if (kf->eouf)
		return 0;

	/* allocate memory to copy data */
	kf->write_buffer_size = count;
	kf->write_index = 0;
	if (allocate_buffer(&kf->write_buffer, &kf->write_buffer_mem_size,
			kf->write_buffer_size))
		goto exit_no_mem;

	/* notify kputc that it can write */
	up(&kf->output);

	/* wait until data is written */
	if (down_interruptible(&kf->write))
		goto exit_interrupt;

	if (copy_to_user(buf, kf->write_buffer, kf->write_index))
		goto exit_bad_copy;

	goto exit_normal;
exit_interrupt:
	/* TODO: could I goto exit_normal and let the user try again? */
	DBG("procfs read function interrupted");
	goto exit_fail;
exit_no_mem:
	DBG("out of memory");
	goto exit_fail;
exit_bad_copy:
	DBG("copy_to_user failed");
	goto exit_fail;
exit_fail:
	kf->write_error = 1;
exit_normal:
	return kf->write_index;
}

/* users write, so used for read mode */
static ssize_t procfs_cb_write(struct file *file, const char __user *buf,
		size_t count, loff_t *ppos)
{
	struct proc_dir_entry *pde = PDE(file->f_path.dentry->d_inode);
	KFILE *kf = pde->data;

	/* wait if data being read */
	if (down_interruptible(&kf->read))
		goto exit_interrupt;

	/* allocate memory to copy data */
	kf->read_buffer_size = count;
	kf->read_index = 0;
	if (allocate_buffer(&kf->read_buffer, &kf->read_buffer_mem_size,
			kf->read_buffer_size))
		goto exit_no_mem;

	/*
	 * not using copy_from_user because read_buffer is dynamically allocated
	 * and that confuses gcc into thinking the buffer size is not correct
	 */
	if (_copy_from_user(kf->read_buffer, buf, count))
		goto exit_bad_copy;

	goto exit_normal;
exit_interrupt:
	/* TODO: could I goto exit_normal and let the user try again? */
	DBG("procfs write function interrupted");
	kf->eokf = true;
	/* It is ok if `kf->input` gets `up`ed twice.  `kgetc` will take care of it */
	goto exit_fail;
exit_no_mem:
	DBG("out of memory");
	goto exit_fail;
exit_bad_copy:
	DBG("copy_from_user failed");
	goto exit_fail;
exit_fail:
	kf->read_error = 1;
	count = 0;
exit_normal:
	/* notify kgetc that it can continue */
	up(&kf->input);
	return count;
}

static int procfs_cb_open(struct inode *i, struct file *file)
{
	struct proc_dir_entry *pde = PDE(file->f_path.dentry->d_inode);
	KFILE *kf = pde->data;

	if (kf->uopen)
		return -EBUSY;
	kclearerr(kf);
	kf->uopen = true;

	return 0;
}

static int procfs_cb_release(struct inode *i, struct file *file)
{
	struct proc_dir_entry *pde = PDE(file->f_path.dentry->d_inode);
	KFILE *kf = pde->data;

	kio_unblock(kf);

	return 0;
}

static struct file_operations fops_r = {
	.open		= procfs_cb_open,
	.write		= procfs_cb_write,
	.release	= procfs_cb_release
};

static struct file_operations fops_w = {
	.open		= procfs_cb_open,
	.read		= procfs_cb_read,
	.release	= procfs_cb_release
};

static struct file_operations fops_rw = {
	.open		= procfs_cb_open,
	.read		= procfs_cb_read,
	.write		= procfs_cb_write,
	.release	= procfs_cb_release
};

void kio_procfs_close(struct kio_procfs *data)
{
	if (data == NULL || data->ent == NULL)
		return;

	remove_proc_entry(data->ent->name, NULL);
}

int kio_procfs_open(struct kio_procfs * KIO_RESTRICT data, const char * KIO_RESTRICT path,
		bool for_read, bool for_write)
{
	umode_t m = (for_read?S_IWUGO:0) | (for_write?S_IRUGO:0) | S_IFREG;
	struct file_operations *op = NULL;
	if (for_read && for_write)
		op = &fops_rw;
	else if (for_read)
		op = &fops_r;
	else if (for_write)
		op = &fops_w;
	else
		goto exit_internal;

	/* TODO: are paths automatically handled? */
	data->ent = proc_create_data(path, m, NULL, op, data->file);
	if (data->ent == NULL)
		goto exit_no_file;
	return 0;
exit_internal:
	LOG("internal error: caught missing read or write in kopen mode too late");
	goto exit_fail;
exit_no_file:
exit_fail:
	return -1;
}
