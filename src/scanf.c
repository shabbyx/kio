/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "internal.h"

/* For scanlist, we need to know what number of charactes we are dealing with */
#ifndef CHAR_MAX
# define CHAR_MAX (__SCHAR_MAX__)
#endif

#define KIO_LEN_DOUBLE_CHAR 10

enum length_modifier
{
	KIO_LM_NONE	= 0,
	KIO_LM_H	= 1,
	KIO_LM_HH	= 1 + KIO_LEN_DOUBLE_CHAR,
	KIO_LM_L	= 2,
	KIO_LM_LL	= 2 + KIO_LEN_DOUBLE_CHAR,
	KIO_LM_Z	= 3,
};

#define KIO_KEEP_WS 0x0400
#define KIO_UNSIGNED 0x0800
#define KIO_0_FIRST 0x1000
#define KIO_X_SECOND 0x2000

enum conversion_specifier
{
	KIO_CS_NONE	= 0,
	KIO_CS_D	= 0x0001,
	KIO_CS_I	= 0x0002 | KIO_0_FIRST | KIO_X_SECOND,
	KIO_CS_O	= 0x0004 | KIO_UNSIGNED | KIO_0_FIRST,
	KIO_CS_U	= 0x0008 | KIO_UNSIGNED,
	KIO_CS_X	= 0x0010 | KIO_UNSIGNED | KIO_0_FIRST | KIO_X_SECOND,
	KIO_CS_P	= 0x0020 | KIO_UNSIGNED | KIO_0_FIRST | KIO_X_SECOND,
	KIO_CS_C	= 0x0040 | KIO_KEEP_WS,
	KIO_CS_S	= 0x0080,
	KIO_CS_REG	= 0x0100 | KIO_KEEP_WS,
	KIO_CS_N	= 0x0200 | KIO_KEEP_WS
};

struct match_item
{
	bool suppress;
	bool use_field_width;
	unsigned int width;
	enum length_modifier length;
	enum conversion_specifier type;
};

/*
 * A few helper functions for vkscanf
 */
static inline bool kio_isspace(char c)
{
	return c == ' ' || c == '\t' || c == '\n' || c == '\r' || c == '\v' || c == '\f';
}

static inline bool kio_is_hex(char c)
{
	return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}

static inline bool kio_is_oct(char c)
{
	return (c >= '0' && c <= '7');
}

static inline bool kio_is_dec(char c)
{
	return (c >= '0' && c <= '9');
}

static inline int kio_get_digit(char c)
{
	if (c >= '0' && c <= '9')
		return (int)(c - '0');
	if (c >= 'a' && c <= 'f')
		return (int)(c - 'a') + 10;
	if (c >= 'A' && c <= 'F')
		return (int)(c - 'A') + 10;
	return 0;
}

static int kio_skip_spaces(KFILE *stream)
{
	int read = 0;
	int c = kgetc(stream);
	while (c != EOKF)
	{
		++read;
		if (!kio_isspace((unsigned char)c))
		{
			kungetc(c, stream);
			--read;
			break;
		}
		c = kgetc(stream);
	}
	return read;
}

static int kio_parse_item(const char ** KIO_RESTRICT p,
		struct match_item * KIO_RESTRICT item)
{
	const char * KIO_RESTRICT cur = *p;
	unsigned int tmp;
	int ret = -1;
	*item = (struct match_item){
		.length = KIO_LM_NONE,
		.type = KIO_CS_NONE,
		.suppress = false,
		.use_field_width = false
	};

	/* check for suppression character */
	if (*cur == '*')
	{
		item->suppress = true;
		++cur;
	}

	/* check for maximum field width */
	if (sscanf(cur, "%u%n", &item->width, &tmp) == 1)
	{
		item->use_field_width = true;
		cur += tmp;
	}

	/* check for length modifier */
	switch (*cur)
	{
	case 'h':
		item->length = KIO_LM_H;
		break;
	case 'l':
		item->length = KIO_LM_L;
		break;
	case 'z':
		item->length = KIO_LM_Z;
		break;
	default:
		break;
	}
	if (item->length != KIO_LM_NONE)
	{
		++cur;
		if ((*cur == 'h' && item->length == KIO_LM_H)
			|| (*cur == 'l' && item->length == KIO_LM_L))
		{
			item->length += KIO_LEN_DOUBLE_CHAR;
			++cur;
		}
	}

	/* check for conversion specifier */
	ret = 0;
	switch (*cur)
	{
	case 'd':
		item->type = KIO_CS_D;
		break;
	case 'i':
		item->type = KIO_CS_I;
		break;
	case 'o':
		item->type = KIO_CS_O;
		break;
	case 'u':
		item->type = KIO_CS_U;
		break;
	case 'x':
	case 'X':
		item->type = KIO_CS_X;
		break;
	case 'p':
		item->type = KIO_CS_P;
		break;
	case 'c':
		item->type = KIO_CS_C;
		break;
	case 's':
		item->type = KIO_CS_S;
		break;
	case '[':
		item->type = KIO_CS_REG;
		break;
	case 'n':
		item->type = KIO_CS_N;
		break;
	default:
		ret = -1;
		break;
	}

	*p = cur;
	return ret;
}

static int kio_match_int(KFILE * KIO_RESTRICT stream, struct match_item * KIO_RESTRICT item,
		long long * KIO_RESTRICT ll, unsigned long long * KIO_RESTRICT ull)
{
	int read = 0;
	int r;
	char c;
	bool negate = false;
	bool is_oct = item->type == KIO_CS_O;
	bool is_hex = item->type == KIO_CS_X || item->type == KIO_CS_P;
	bool first = true;
	bool second = false;
	bool first_was_0 = false;
	bool sign_seen = false;
	unsigned int to_read = item->width;

#define APPLY_DIGIT(d, b)				\
	do {						\
		if (ll)					\
			*ll = *ll * (b) + (d);		\
		if (ull)				\
			*ull = *ull * (b) + (d);	\
	} while (0)					\

	APPLY_DIGIT(0, 0);

	while (true)
	{
		if (item->use_field_width && to_read == 0)
			break;

		/* Read one char */
		r = kgetc(stream);
		if (r == EOKF)
			break;
		c = (unsigned char)r;

		++read;
		if (item->use_field_width)
			--to_read;

		if (first && !sign_seen && (c == '+' || c == '-'))
		{
			/* Could be a sign in the first place */
			if (c == '-')
				negate = true;
			sign_seen = true;
		}
		else
		{
			if (first && c == '0' && (item->type & KIO_0_FIRST))
			{
				/*
				 * Or a zero in the first place, if the specifier allows it
				 * If specifier is %i, autodetect format
				 */
				if (item->type == KIO_CS_I)
					is_oct = true;
				first_was_0 = true;
			}
			else if (second && first_was_0 && (c == 'x' || c == 'X') && (item->type & KIO_X_SECOND))
			{
				/*
				 * If the first digit was a zero, and the second x or X,
				 * then the number is hex
				 * If specifier is %i, autodetect format
				 */
				if (item->type == KIO_CS_I)
				{
					is_oct = false;
					is_hex = true;
				}
			}
			else
			{
				/* Otherwise, it must be a digit */
				int d = kio_get_digit(c);
				if (is_hex && kio_is_hex(c))
					APPLY_DIGIT(d, 16);
				else if (is_oct && kio_is_oct(c))
					APPLY_DIGIT(d, 8);
				else if (kio_is_dec(c))
					APPLY_DIGIT(d, 10);
				else
				{
					/* if not a digit, then end of matching */
					kungetc(c, stream);
					--read;
					/* if no digits could be matched, it should be a fail */
					if (first || (second && sign_seen))
						goto exit_no_match;
					break;
				}
			}

			second = false;
			if (first)
				second = true;
			first = false;
		}
	}
	if (negate)
		APPLY_DIGIT(0, -1);
#undef APPLY_DIGIT

	return read;
exit_no_match:
	return 0;
}

static int kio_match_char(KFILE * KIO_RESTRICT stream, struct match_item * KIO_RESTRICT item,
		char * KIO_RESTRICT c)
{
	int r;

	r = kgetc(stream);
	if (r == EOKF)
		goto exit_eokf;
	*c = (unsigned char)r;

	return 1;
exit_eokf:
	return 0;
}

static int kio_match_scanset(KFILE * KIO_RESTRICT stream,
		struct match_item * KIO_RESTRICT item, char * KIO_RESTRICT s,
		bool * KIO_RESTRICT scanset)
{
	int read = 0;
	int r;
	char c;
	unsigned int to_read = item->width;

	while (true)
	{
		if (item->use_field_width && to_read == 0)
			break;

		/* Read one char */
		r = kgetc(stream);
		if (r == EOKF)
			break;
		c = (unsigned char)r;

		++read;
		if (item->use_field_width)
			--to_read;

		/*
		 * Make sure that the character is in the range of scanset
		 * and is indeed accepted by the scanset
		 */
		if (c > CHAR_MAX || c < 0 || !scanset[(unsigned int)c])
		{
			kungetc(c, stream);
			--read;
			break;
		}

		if (s)
			*s++ = c;
	}

	if (s)
		*s = '\0';
	return read;
}

static int kio_match_str(KFILE * KIO_RESTRICT stream, struct match_item * KIO_RESTRICT item,
		char * KIO_RESTRICT s)
{
	bool scanset[CHAR_MAX + 1] = { false };
	unsigned int i;

	/* scanset of %s is all non-white-space characters */
	for (i = '!'; i <= CHAR_MAX; ++i)
		scanset[i] = true;

	return kio_match_scanset(stream, item, s, scanset);
}

static int kio_match_reg(KFILE * KIO_RESTRICT stream, struct match_item * KIO_RESTRICT item,
		const char ** KIO_RESTRICT p, char * KIO_RESTRICT s)
{
	bool scanset[CHAR_MAX + 1] = {0};
	unsigned int i;
	int ret;
	const char * KIO_RESTRICT cur = *p;
	bool negate = false;
	bool first = true;
	bool second = false;
	char range_begin = '\0';

	/* ignore the '[' of "%[" */
	++cur;

	while (true)
	{
		if (*cur == '\0')
			goto exit_bad_format;
		else if (first && *cur == '^')
			negate = true;
		else if (((first || (second && negate))
					&& (*cur == ']' || *cur == '-'))	/* initial ] or -	 */
			|| (*cur == '-' && *(cur + 1) == ']')			/* or last -		 */
			|| (*cur != '-' && *cur != ']'))			/* or other than - and ] */
			scanset[(unsigned int)*cur] = true;
		else if (*cur == '-')
		{
			++cur;
			if (*cur == '\0')
				goto exit_bad_format;
			while (range_begin <= *cur)
				scanset[(unsigned int)range_begin++] = true;
		}
		else if (*cur == ']')
		{
			++cur;
			break;
		}
		else
			goto exit_internal;

		range_begin = *cur;
		++cur;
		second = false;
		if (first)
			second = true;
		first = false;
	}

	if (negate)
		for (i = 1; i <= CHAR_MAX; ++i)
			scanset[i] = !scanset[i];

	ret = kio_match_scanset(stream, item, s, scanset);
	goto exit_common;
exit_bad_format:
	goto exit_fail;
exit_internal:
	DBG("internal error: not all regular expression cases were accounted for");
	goto exit_fail;
exit_fail:
	ret = 0;
exit_common:
	/* leave the last character read (for `for` to increment) */
	if (*p != cur)
		--cur;
	*p = cur;
	return ret;
}

#define ASSIGN_NEXT_SIGNED(v)							\
do {										\
	switch (item.length)							\
	{									\
	case KIO_LM_NONE:							\
	default:								\
		*va_arg(arg, int *) = (int)(v);					\
		break;								\
	case KIO_LM_H:								\
		*va_arg(arg, short *) = (short)(v);				\
		break;								\
	case KIO_LM_HH:								\
		*va_arg(arg, char *) = (char)(v);				\
		break;								\
	case KIO_LM_L:								\
		*va_arg(arg, long *) = (long)(v);				\
		break;								\
	case KIO_LM_LL:								\
		*va_arg(arg, long long *) = (long long)(v);			\
		break;								\
	case KIO_LM_Z:								\
		*va_arg(arg, ssize_t *) = (ssize_t)(v);				\
		break;								\
	}									\
} while (0)

#define ASSIGN_NEXT_UNSIGNED(v)							\
do {										\
	switch (item.length)							\
	{									\
	case KIO_LM_NONE:							\
	default:								\
		*va_arg(arg, unsigned int *) = (unsigned int)(v);		\
		break;								\
	case KIO_LM_H:								\
		*va_arg(arg, unsigned short *) = (unsigned short)(v);		\
		break;								\
	case KIO_LM_HH:								\
		*va_arg(arg, unsigned char *) = (unsigned char)(v);		\
		break;								\
	case KIO_LM_L:								\
		*va_arg(arg, unsigned long *) = (unsigned long)(v);		\
		break;								\
	case KIO_LM_LL:								\
		*va_arg(arg, unsigned long long *) = (unsigned long long)(v);	\
		break;								\
	case KIO_LM_Z:								\
		*va_arg(arg, size_t *) = (size_t)(v);				\
		break;								\
	}									\
} while (0)

__scanf(2, 3) int kscanf(KFILE * KIO_RESTRICT stream, const char * KIO_RESTRICT format, ...)
{
	int ret;
	va_list args;

	va_start(args, format);
	ret = vkscanf(stream, format, args);
	va_end(args);

	return ret;
}
EXPORT_SYMBOL(kscanf);

__scanf(2, 0) int vkscanf(KFILE * KIO_RESTRICT stream, const char * KIO_RESTRICT format, va_list arg)
{
	const char *p;
	int num_read = 0;
	int chars_read = 0;

	for (p = format; *p; ++p)
		if (kio_isspace(*p))
			chars_read += kio_skip_spaces(stream);
		else if (*p != '%' || (*p == '%' && *(p + 1) == '%'))
		{
			int c = kgetc(stream);
			if (c == EOKF)
				goto exit_eokf;
			++chars_read;

			if (*p == '%')
				++p;
			if (*p != (unsigned char)c)
				goto exit_no_match;
		}
		else
		{
			int ret;
			struct match_item item;
			long long ll;
			unsigned long long ull;
			char *str = NULL;
			char c;

			/* parse the directive */
			++p;
			ret = kio_parse_item(&p, &item);
			if (ret || item.type == KIO_CS_NONE)
				goto exit_no_match;

			/* ignore white space for all except c, reg and n */
			if (!(item.type & KIO_KEEP_WS))
				chars_read += kio_skip_spaces(stream);

			/* match the item */
			switch (item.type)
			{
			case KIO_CS_D:
			case KIO_CS_I:
				if ((ret = kio_match_int(stream, &item, &ll, NULL)) == 0)
					goto exit_no_match;

				if (!item.suppress)
					ASSIGN_NEXT_SIGNED(ll);
				chars_read += ret;
				break;
			case KIO_CS_O:
			case KIO_CS_U:
			case KIO_CS_X:
				if ((ret = kio_match_int(stream, &item, NULL, &ull)) == 0)
					goto exit_no_match;

				if (!item.suppress)
					ASSIGN_NEXT_UNSIGNED(ull);
				chars_read += ret;
				break;
			case KIO_CS_P:
				if ((ret = kio_match_int(stream, &item, NULL, &ull)) == 0)
					goto exit_no_match;

				if (!item.suppress)
					*va_arg(arg, void **) = (void *)(unsigned long)ull;
				chars_read += ret;
				break;
			case KIO_CS_C:
				if ((ret = kio_match_char(stream, &item, &c)) == 0)
					goto exit_no_match;

				if (!item.suppress)
					*va_arg(arg, char *) = c;
				chars_read += ret;
				break;
			case KIO_CS_S:
				if (!item.suppress)
					str = va_arg(arg, char *);
				if ((ret = kio_match_str(stream, &item, str)) == 0)
					goto exit_no_match;

				chars_read += ret;
				break;
			case KIO_CS_REG:
				if (!item.suppress)
					str = va_arg(arg, char *);
				if ((ret = kio_match_reg(stream, &item, &p, str)) == 0)
					goto exit_no_match;

				chars_read += ret;
				break;
			case KIO_CS_N:
				/* %n can't have field width or suppression character */
				if (item.use_field_width || item.suppress)
					goto exit_no_match;

				ASSIGN_NEXT_SIGNED(chars_read);
				break;
			default:
				goto exit_no_match;
			}

			if (item.type != KIO_CS_N && !item.suppress)
				++num_read;
		}
exit_eokf:
	return num_read == 0?EOKF:num_read;
exit_no_match:
	return num_read;
}
EXPORT_SYMBOL(vkscanf);
