/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KIO_PROCFS_H
#define KIO_PROCFS_H

#include "internal.h"

void kio_procfs_close(struct kio_procfs *data);
int kio_procfs_open(struct kio_procfs * KIO_RESTRICT data, const char * KIO_RESTRICT path,
		bool for_read, bool for_write);	/* returns 0 if successful */

#endif
