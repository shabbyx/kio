/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "internal.h"

#define KIO_LEN_DOUBLE_CHAR 10

enum length_modifier
{
	KIO_LM_NONE	= 0,
	KIO_LM_H	= 1,
	KIO_LM_HH	= 1 + KIO_LEN_DOUBLE_CHAR,
	KIO_LM_L	= 2,
	KIO_LM_LL	= 2 + KIO_LEN_DOUBLE_CHAR,
	KIO_LM_Z	= 3,
};

#define KIO_NOT_NUM 0x0200
#define KIO_UNSIGNED 0x0400
#define KIO_0_FIRST 0x0800
#define KIO_X_SECOND 0x1000

enum conversion_specifier
{
	KIO_CS_NONE	= 0,
	KIO_CS_D_I	= 0x0001,
	KIO_CS_O	= 0x0002 | KIO_UNSIGNED | KIO_0_FIRST,
	KIO_CS_U	= 0x0004 | KIO_UNSIGNED,
	KIO_CS_X	= 0x0008 | KIO_UNSIGNED | KIO_0_FIRST | KIO_X_SECOND,
	KIO_CS_X_CAP	= 0x0010 | KIO_UNSIGNED | KIO_0_FIRST | KIO_X_SECOND,	/* capital X */
	KIO_CS_P	= 0x0020 | KIO_UNSIGNED | KIO_0_FIRST | KIO_X_SECOND,
	KIO_CS_C	= 0x0040 | KIO_NOT_NUM,
	KIO_CS_S	= 0x0080 | KIO_NOT_NUM,
	KIO_CS_N	= 0x0100
};

struct print_item
{
	bool left_justify;
	bool prepend_sign;
	bool prepend_space;
	bool prepend_zero;
	bool alternative_form;
	bool load_width_from_arg;
	bool load_precision_from_arg;
	int width;
	int precision;
	enum length_modifier length;
	enum conversion_specifier type;
};

static int kio_parse_item(const char ** KIO_RESTRICT p,
		struct print_item * KIO_RESTRICT item)
{
	const char * KIO_RESTRICT cur = *p;
	unsigned int tmp;
	bool flags_finished;
	int ret = -1;
	*item = (struct print_item){
		.left_justify = false,
		.prepend_sign = false,
		.prepend_space = false,
		.prepend_zero = false,
		.alternative_form = false,
		.load_width_from_arg = false,
		.load_precision_from_arg = false,
		.width = 1,
		.precision = -1,
		.length = KIO_LM_NONE,
		.type = KIO_CS_NONE
	};

	/* check for flag characters */
	flags_finished = false;
	do
	{
		switch (*cur)
		{
		case '-':
			item->left_justify = true;
			item->prepend_zero = false;
			++cur;
			break;
		case '+':
			item->prepend_sign = true;
			item->prepend_space = false;
			++cur;
			break;
		case ' ':
			if (!item->prepend_sign)
				item->prepend_space = true;
			++cur;
			break;
		case '#':
			item->alternative_form = true;
			++cur;
			break;
		case '0':
			if (!item->left_justify)
				item->prepend_zero = true;
			++cur;
			break;
		default:
			flags_finished = true;
			break;
		}
	} while (!flags_finished);

	/* check for minimum field width */
	if (*cur == '*')
	{
		item->load_width_from_arg = true;
		++cur;
	}
	else if (sscanf(cur, "%d%n", &item->width, &tmp) == 1)
		cur += tmp;

	/* check for precision */
	if (*cur == '.')
	{
		item->prepend_zero = false;
		++cur;
		if (*cur == '*')
		{
			item->load_precision_from_arg = true;
			++cur;
		}
		else if (sscanf(cur, "%d%n", &item->precision, &tmp) == 1)
			cur += tmp;
		else
			item->precision = 0;
	}

	/* check for length modifier */
	switch (*cur)
	{
	case 'h':
		item->length = KIO_LM_H;
		break;
	case 'l':
		item->length = KIO_LM_L;
		break;
	case 'z':
		item->length = KIO_LM_Z;
		break;
	default:
		break;
	}
	if (item->length != KIO_LM_NONE)
	{
		++cur;
		if ((*cur == 'h' && item->length == KIO_LM_H)
			|| (*cur == 'l' && item->length == KIO_LM_L))
		{
			item->length += KIO_LEN_DOUBLE_CHAR;
			++cur;
		}
	}

	/* check for conversion specifier */
	ret = 0;
	switch (*cur)
	{
	case 'd':
	case 'i':
		item->type = KIO_CS_D_I;
		break;
	case 'o':
		item->type = KIO_CS_O;
		break;
	case 'u':
		item->type = KIO_CS_U;
		break;
	case 'x':
		item->type = KIO_CS_X;
		break;
	case 'X':
		item->type = KIO_CS_X_CAP;
		break;
	case 'p':
		item->type = KIO_CS_P;
		break;
	case 'c':
		item->type = KIO_CS_C;
		break;
	case 's':
		item->type = KIO_CS_S;
		break;
	case 'n':
		item->type = KIO_CS_N;
		break;
	default:
		ret = -1;
		break;
	}

	/* default precision, -1 is a flag for %s */
	if (item->type != KIO_CS_S && item->precision == -1)
		item->precision = 1;
	/* default alternative form for %p */
	if (item->type == KIO_CS_P)
		item->alternative_form = true;

	*p = cur;
	return ret;
}

static int kio_print_buf(KFILE * KIO_RESTRICT stream, struct print_item * KIO_RESTRICT item,
		const char * KIO_RESTRICT s)
{
	int wrote = 0;
	size_t len;			/* length of given string */
	size_t extended_len = 0;	/* if number: len + added 0s for precision */
	char extras[5];			/* sign, 0 or 0x */
	size_t extras_len = 0;
	size_t padding = 0;		/* padding if not left justified */
	char pad_char = ' ';
	bool pad_first = true;
	size_t i;

#define WRITE_CHAR(c)					\
	do {						\
		int temp = kputc(c, stream);		\
		if (temp == EOKF)			\
			goto exit_io_error;		\
		++wrote;				\
	} while (0)

	/* fill in extras, moving - of signed integers to extra */
	if (s[0] != '-')
	{
		if (item->prepend_sign)
			extras[extras_len++] = '+';
		else if (item->prepend_space)
			extras[extras_len++] = ' ';
	}
	else if (item->type == KIO_CS_D_I)
	{
		extras[extras_len++] = '-';
		++s;
	}
	if (item->alternative_form)
	{
		if (item->type & KIO_0_FIRST)
			extras[extras_len++] = '0';
		if (item->type & KIO_X_SECOND)
			extras[extras_len++] = item->type == KIO_CS_X_CAP?'X':'x';
	}

	/* find length of given string */
	if (item->type != KIO_CS_S || item->precision == -1)
		len = strlen(s);
	else
		len = item->precision;

	/* if is a number and precision is bigger than length, 0s need to be added */
	if (!(item->type & KIO_NOT_NUM) && item->precision > (int)len)
	{
		extended_len = item->precision;
		/* in case of %#o, one 0 is already included in extras */
		if (item->alternative_form && item->type == KIO_CS_O)
			--extended_len;
	}
	else
		extended_len = len;

	/* if not left justified, there could be padding */
	if (!item->left_justify && item->width > extended_len + extras_len)
	{
		padding = item->width - extended_len;

		/* if prepending zero, first write extras and then padding */
		if (item->prepend_zero)
		{
			pad_char = '0';
			pad_first = false;
		}
	}

	/* write padding and extras */
	if (pad_first)
		for (i = 0; i < padding; ++i)
			WRITE_CHAR(pad_char);
	for (i = 0; i < extras_len; ++i)
		WRITE_CHAR(extras[i]);
	if (!pad_first)
		for (i = 0; i < padding; ++i)
			WRITE_CHAR(pad_char);

	/* if required, write 0s to get to precision */
	for (i = len; i < extended_len; ++i)
		WRITE_CHAR('0');

	/* write the given string */
	for (i = 0; i < len; ++i)
		WRITE_CHAR(s[i]);

	/* if left justified, write right padding */
	if (item->left_justify)
		for (i = extended_len + extras_len; i < item->width; ++i)
			WRITE_CHAR(' ');
#undef WRITE_CHAR

	return wrote;
exit_io_error:
	return -1;
}

/*
 * Note: I don't see any reliable way of determining the number of bits in size_t
 * However, in the kernel, they are defined based on BITS_PER_LONG.  So, unless
 * anything strange happens later in the kernel, this should be alright.
 */
#if BITS_PER_LONG != 64
# define PROMOTED_SSIZE_T int
# define PROMOTED_SIZE_T unsigned int
#else
# define PROMOTED_SSIZE_T ssize_t
# define PROMOTED_SIZE_T size_t
#endif

#define PRINT_NEXT_SIGNED(cs)							\
do {										\
	switch (item.length)							\
	{									\
	case KIO_LM_NONE:							\
	default:								\
		sprintf(printed, "%"cs, va_arg(arg, int));			\
		break;								\
	case KIO_LM_H:								\
		sprintf(printed, "%h"cs, va_arg(arg, int));			\
		break;								\
	case KIO_LM_HH:								\
		sprintf(printed, "%hh"cs, va_arg(arg, int));			\
		break;								\
	case KIO_LM_L:								\
		sprintf(printed, "%l"cs, va_arg(arg, long));			\
		break;								\
	case KIO_LM_LL:								\
		sprintf(printed, "%ll"cs, va_arg(arg, long long));		\
		break;								\
	case KIO_LM_Z:								\
		sprintf(printed, "%z"cs, va_arg(arg, PROMOTED_SSIZE_T));	\
		break;								\
	}									\
} while (0)

#define PRINT_NEXT_UNSIGNED(cs)							\
do {										\
	switch (item.length)							\
	{									\
	case KIO_LM_NONE:							\
	default:								\
		sprintf(printed, "%"cs, va_arg(arg, unsigned int));		\
		break;								\
	case KIO_LM_H:								\
		sprintf(printed, "%h"cs, va_arg(arg, unsigned int));		\
		break;								\
	case KIO_LM_HH:								\
		sprintf(printed, "%hh"cs, va_arg(arg, unsigned int));		\
		break;								\
	case KIO_LM_L:								\
		sprintf(printed, "%l"cs, va_arg(arg, unsigned long));		\
		break;								\
	case KIO_LM_LL:								\
		sprintf(printed, "%ll"cs, va_arg(arg, unsigned long long));	\
		break;								\
	case KIO_LM_Z:								\
		sprintf(printed, "%z"cs, va_arg(arg, PROMOTED_SIZE_T));		\
		break;								\
	}									\
} while (0)

__printf(2, 3) int kprintf(KFILE * KIO_RESTRICT stream, const char * KIO_RESTRICT format, ...)
{
	int ret;
	va_list args;

	va_start(args, format);
	ret = vkprintf(stream, format, args);
	va_end(args);

	return ret;
}
EXPORT_SYMBOL(kprintf);

__printf(2, 0) int vkprintf(KFILE * KIO_RESTRICT stream, const char * KIO_RESTRICT format, va_list arg)
{
	const char *p;
	int chars_wrote = 0;

	for (p = format; *p; ++p)
		if (*p != '%' || (*p == '%' && *(p + 1) == '%'))
		{
			int c = kputc(*p, stream);
			if (c == EOKF)
				goto exit_eokf;
			++chars_wrote;

			if (*p == '%')
				++p;
		}
		else
		{
			int ret;
			size_t len;
			struct print_item item;
			char printed[30];	/* maximum possible is ~25 digits including additional signs */
			const char *to_print;

			/* parse the directive */
			++p;
			ret = kio_parse_item(&p, &item);
			if (ret || item.type == KIO_CS_NONE)
				goto exit_bad_format;

			/* get width if required */
			if (item.load_width_from_arg)
				item.width = va_arg(arg, int);
			if (item.width < 0)
			{
				item.width = -item.width;
				item.left_justify = true;
				item.prepend_zero = false;
			}

			/* get precision if required */
			if (item.load_precision_from_arg)
			{
				item.precision = va_arg(arg, int);
				if (item.precision < 0)
					item.precision = 0;
			}

			/* possibly convert the item */
			to_print = printed;
			switch (item.type)
			{
			case KIO_CS_D_I:
				PRINT_NEXT_SIGNED("d");
				break;
			case KIO_CS_O:
				PRINT_NEXT_SIGNED("o");
				break;
			case KIO_CS_U:
				PRINT_NEXT_SIGNED("u");
				break;
			case KIO_CS_X:
				PRINT_NEXT_SIGNED("x");
				break;
			case KIO_CS_X_CAP:
				PRINT_NEXT_SIGNED("X");
				break;
			case KIO_CS_P:
				sprintf(printed, "%lx", (unsigned long)va_arg(arg, void *));
				break;
			case KIO_CS_C:
				printed[0] = (unsigned char)va_arg(arg, int);
				printed[1] = '\0';
				break;
			case KIO_CS_S:
				to_print = va_arg(arg, const char *);
				break;
			case KIO_CS_N:
				to_print = NULL;
				break;
			default:
				goto exit_io_error;
			}

			/* adjust flags */
			if (item.type & KIO_NOT_NUM)
			{
				item.prepend_sign = false;
				item.prepend_space = false;
				item.prepend_zero = false;
			}
			if (item.type & KIO_UNSIGNED)
			{
				item.prepend_sign = false;
				item.prepend_space = false;
				if (item.type & KIO_0_FIRST)
				{
					len = strlen(printed);
					if (item.alternative_form && len >= item.precision)
					{
						if (printed[0] == '0')
						{
							item.precision = len;
							item.alternative_form = false;
						}
						else
							item.precision = len + (item.type & KIO_X_SECOND?2:1);
					}
				}
			}

			/* output the buffer if not %n, or write chars_wrote */
			if (item.type != KIO_CS_N)
			{
				if ((ret = kio_print_buf(stream, &item, to_print)) < 0)
					goto exit_io_error;

				chars_wrote += ret;
			}
			else
			{
				switch (item.length)
				{
				case KIO_LM_NONE:
				default:
					*va_arg(arg, int *) = (int)chars_wrote;
					break;
				case KIO_LM_H:
					*va_arg(arg, short *) = (short)chars_wrote;
					break;
				case KIO_LM_HH:
					*va_arg(arg, char *) = (char)chars_wrote;
					break;
				case KIO_LM_L:
					*va_arg(arg, long *) = (long)chars_wrote;
					break;
				case KIO_LM_LL:
					*va_arg(arg, long long *) = (long long)chars_wrote;
					break;
				case KIO_LM_Z:
					*va_arg(arg, ssize_t *) = (ssize_t)chars_wrote;
					break;
				}
			}
		}
	return chars_wrote;
exit_bad_format:
exit_eokf:
exit_io_error:
	return -1;
}
EXPORT_SYMBOL(vkprintf);
