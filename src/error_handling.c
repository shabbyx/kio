/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "internal.h"

void kclearerr(KFILE *stream)
{
	stream->eokf = false;
	stream->read_error = false;
	stream->write_error = false;
}
EXPORT_SYMBOL(kclearerr);

int keokf(KFILE *stream)
{
	return stream->eokf && stream->ungot == 0;
}
EXPORT_SYMBOL(keokf);

int kerror(KFILE *stream)
{
	return stream->read_error || stream->write_error;
}
EXPORT_SYMBOL(kerror);

int krerror(KFILE *stream)
{
	return stream->read_error;
}
EXPORT_SYMBOL(krerror);

int kwerror(KFILE *stream)
{
	return stream->write_error;
}
EXPORT_SYMBOL(kwerror);
