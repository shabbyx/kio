/*
 * Copyright (C) 2012-2015  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of kio.
 *
 * kio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * kio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kio.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KIO_INTERNAL_H
#define KIO_INTERNAL_H

#include <linux/semaphore.h>
#include <linux/proc_fs.h>
#include <linux/version.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(3,2,0)
# include <linux/module.h>
#else
# include <linux/export.h>
#endif
#include <kio.h>
#include "config.h"

#ifndef NDEBUG
# define LOG(fmt, ...) printk(KERN_INFO "kio: ..%s(%u): "fmt"\n",			\
			__FILE__ + (sizeof(__FILE__) < 25?0:(sizeof(__FILE__) - 25)),	\
			__LINE__, ##__VA_ARGS__)
# define DBG(fmt, ...) LOG(fmt, ##__VA_ARGS__)
#else
# define LOG(fmt, ...) printk(KERN_INFO "kio: "fmt"\n", ##__VA_ARGS__)
# define DBG(fmt, ...) ((void)0)
#endif

struct kio_procfs
{
	KFILE *file;			/* access back to KFILE */
	struct proc_dir_entry *ent;
};

struct kio_sysfs
{
	KFILE *file;
	/* Here goes data specific to sysfs */
};

struct kio_devfs
{
	KFILE *file;
	/* Here goes data specific to devfs */
};

/* TODO: once done, add a generic method also, so a user can provide the buffer
 * in whatever way she desires, instead of automatically from a file handler.
 * Then, maybe change the proc, sys and dev handlers to use the same interface?
 */

enum kio_fs_type
{
	KIO_PROCFS = 0,
	KIO_SYSFS,
	KIO_DEVFS,
	KIO_INVALID
};

union kio_fs
{
	struct kio_procfs proc;
	struct kio_sysfs sys;
	struct kio_devfs dev;
};

#define KIO_KUNGETC_CHARS 2

/*
 * the number of characters that can be `kungetc`ed should be at least 2.  This
 * is so that `kscanf` can `kungetc` and the user can also `kungetc` and it
 * should still work.
 */
#if KIO_KUNGETC_CHARS < 2
# undef KIO_KUNGETC_CHARS
# define KIO_KUNGETC_CHARS 2
#endif

/*
 * kio has less detail than stdio but on the other hand is used in kernel space.
 * Therefore, this structure is simpler than `FILE` in some aspects and possibly
 * more complicated in others.
 *
 * It is possible for a file to be opened in both read and write modes,
 * therefore, this struct supports both at the same time.
 *
 * To simplify matters, each file can only be opened once at a time by users.
 * If the file is opened in both read and write modes, in theory, it should be
 * able to be opened in both modes by the user (but only once in each mode).
 * However, currently I don't know how to detect in open callback of file_operations
 * whether the user has opened the file in read or write mode.
 *
 * It is possible for the file to be opened by the user once the previous one has
 * finished working with it.  However, currently, the module will possibly see an
 * EOKF which is removed once the file is reopened.  Perhaps it would be a good idea
 * to notify the module working with the KFILE that it has been reopened by the user?
 */
struct _KIO_FILE
{
	bool open;			/* whether the file is open (in kernel) */
	bool binary;			/* if opened in binary mode */
	bool read_mode;			/* if opened for kernel read */
	bool write_mode;		/* if opened for kernel write */
	bool uopen;			/* if user opened the file */

	bool eokf;			/* if end of stream is reached (for kernel, only in read mode) */
	bool eouf;			/* if end of stream is reached (for user, only in write mode) */
	bool read_error;		/* if there has been an error (if file opened in read mode) */
	bool write_error;		/* if there has been an error (if file opened in write mode) */

	enum kio_fs_type type;		/* which subsystem is used (used to choose from `fs`) */
	union kio_fs fs;

	struct semaphore input;		/* to wait for next frame to arrive */
	struct semaphore read;		/* to wait until current frame is consumed */
	struct semaphore output;	/* to wait for next frame to be sent */
	struct semaphore write;		/* to wait until current frame is filled */

	char *read_buffer;		/* copy of buffer from user*/
	size_t read_buffer_mem_size;	/* size of memory allocated for read_buffer (if 0, pointer to someone else's buffer) */
	size_t read_buffer_size;	/* size of current data in read_buffer */
	size_t read_index;		/* read so far */
	char *write_buffer;		/* buffer being filled for user */
	size_t write_buffer_mem_size;	/* size of memory allocated for write_buffer (if 0, pointer to someone else's buffer) */
	size_t write_buffer_size;	/* size of available space in write_buffer (useful when mem_size is 0) */
	size_t write_index;		/* written so far */

	char ungot_chars[KIO_KUNGETC_CHARS]; /* the `kungetc`ed characters */
	size_t ungot;			/* number of `kungetc`ed characters */
};

/* upon file close, unblocks kgetc and kputc to return with a failure */
void kio_unblock(KFILE *kf);

#endif
